FROM openjdk:8
ADD ./target/sif.jar sif.jar
EXPOSE 8085
ENTRYPOINT ["java","-jar","sif.jar"]
