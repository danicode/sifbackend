package com.umsa.sif.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.umsa.sif.dto.CargoEstudiantil;
import com.umsa.sif.dto.crit.CritCargoEstudiantil;

@Repository
public interface CargoEstudiantilDAO {
	List <CargoEstudiantil> listarCargoEstudiantil(CritCargoEstudiantil critCargoEstudiantil);
}
