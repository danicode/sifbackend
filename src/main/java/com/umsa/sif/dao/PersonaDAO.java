package com.umsa.sif.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.umsa.sif.dto.Persona;
import com.umsa.sif.dto.crit.CritPersona;

@Repository
public interface PersonaDAO {
	List <Persona> listarPersona(CritPersona Persona);
	Integer actualizarPersona(Persona persona);
	Integer registrarPersona(Persona persona);
	Integer eliminarPersona(Persona persona);
}
