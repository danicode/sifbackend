package com.umsa.sif.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.umsa.sif.dto.Estudiante;
import com.umsa.sif.dto.crit.CritEstudiante;

@Repository
public interface EstudianteDAO {
	List <Estudiante> listarEstudiante(CritEstudiante critEstudiante);
	Integer registrarEstudiante(Estudiante estudiante);
	Integer actualizarEstudiante(Estudiante estudiante);
	Integer eliminarEstudiante(Estudiante estudiante);
}
