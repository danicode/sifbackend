package com.umsa.sif.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.umsa.sif.dto.TipoCargoNro;

@Repository
public interface TipoCargoNroDAO {
	List <TipoCargoNro> listarTipoCargoNro();
}
