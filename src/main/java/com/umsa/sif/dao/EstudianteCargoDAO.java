package com.umsa.sif.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.umsa.sif.dto.EstudianteCargo;
import com.umsa.sif.dto.crit.CritEstudianteCargo;

@Repository
public interface EstudianteCargoDAO {
	List <EstudianteCargo> listarEstudianteCargo(CritEstudianteCargo critEstudianteCargo);
	List <EstudianteCargo> listarTotalAutoridades(CritEstudianteCargo critEstudianteCargo);
	Integer registrarEstudianteCargo(EstudianteCargo estudianteCargo);
	Integer actualizarEstudianteCargo(EstudianteCargo estudianteCargo);
	Integer eliminarEstudianteCargo(EstudianteCargo estudianteCargo);
	
}
