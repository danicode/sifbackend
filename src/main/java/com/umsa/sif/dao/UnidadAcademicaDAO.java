package com.umsa.sif.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.umsa.sif.dto.UnidadAcademica;
import com.umsa.sif.dto.crit.CritUnidadAcademica;

@Repository
public interface UnidadAcademicaDAO {
	List <UnidadAcademica> listarUnidadAcademica(CritUnidadAcademica critUnidadAcademica);
//	Integer registrarUsuario(AdmUsuario admUsuario);
//	Integer actualizarUsuario(AdmUsuario admUsuario);
//	Integer eliminarUsuario(AdmUsuario admUsuario);
}
