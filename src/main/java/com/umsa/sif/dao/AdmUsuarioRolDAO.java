package com.umsa.sif.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.umsa.sif.dto.AdmUsuarioRol;
import com.umsa.sif.dto.crit.CritAdmUsuarioRol;

@Repository
public interface AdmUsuarioRolDAO {
	List <AdmUsuarioRol> listarUsuarioRol(CritAdmUsuarioRol critAdmUsuarioRol);
	Integer registrarUsuarioRol(AdmUsuarioRol admUsuarioRol);
	Integer eliminarUsuarioRol(AdmUsuarioRol admUsuarioRol);
}
