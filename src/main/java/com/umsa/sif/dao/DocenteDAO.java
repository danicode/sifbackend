package com.umsa.sif.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.umsa.sif.dto.Docente;
import com.umsa.sif.dto.crit.CritDocente;

@Repository
public interface DocenteDAO {
	List <Docente> listarDocente(CritDocente critDocente);
	Integer registrarDocente(Docente docente);
	Integer actualizarDocente(Docente docente);
	Integer eliminarDocente(Docente docente);
}
