package com.umsa.sif.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.umsa.sif.dto.TipoCargoNivel;

@Repository
public interface TipoCargoNivelDAO {
	List <TipoCargoNivel> listarTipoCargoNivel();
}
