package com.umsa.sif.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.umsa.sif.dto.AdmUsuarioUnidad;
import com.umsa.sif.dto.crit.CritAdmUsuarioUnidad;

@Repository
public interface AdmUsuarioUnidadDAO {
	List <AdmUsuarioUnidad> listarUsuarioUnidad(CritAdmUsuarioUnidad critAdmUsuarioUnidad);
	Integer registrarUsuarioUnidad(AdmUsuarioUnidad admUsuarioUnidad);
	Integer eliminarUsuarioUnidad(AdmUsuarioUnidad admUsuarioUnidad);
}
