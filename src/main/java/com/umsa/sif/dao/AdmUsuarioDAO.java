package com.umsa.sif.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.umsa.sif.dto.AdmUsuario;
import com.umsa.sif.dto.crit.CritAdmUsuario;

@Repository
public interface AdmUsuarioDAO {
	List <AdmUsuario> listarUsuarios(CritAdmUsuario critAdmUsuario);
	Integer registrarUsuario(AdmUsuario admUsuario);
	Integer registrarUsuarioLDAP(AdmUsuario admUsuario);
	Integer actualizarUsuario(AdmUsuario admUsuario);
	Integer eliminarUsuario(AdmUsuario admUsuario);
}
