package com.umsa.sif.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.umsa.sif.dto.DocenteCargo;
import com.umsa.sif.dto.crit.CritDocenteCargo;

@Repository
public interface DocenteCargoDAO {
	List <DocenteCargo> listarDocenteCargo(CritDocenteCargo critDocenteCargo);
	Integer registrarDocenteCargo(DocenteCargo docenteCargo);
	Integer actualizarDocenteCargo(DocenteCargo docenteCargo);
	Integer eliminarDocenteCargo(DocenteCargo docenteCargo);
	
}
