package com.umsa.sif.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ocontreras
 */
public class WsEstudiante implements Serializable {
    private Long idEstudiante;
    
    //// PROPIEDADES DE PERSONA
    private Long idPersona;
    private Integer idEcivil;
    private String estadoCivil;
    private Integer idSexo;
    private String sexo;
    private String paterno;
    private String materno;
    private String nombres;
    private String casada;
    private Date nacFecha;
    private Integer idNacProvin;
    private String nacProvin;
    private String nacLocal;
    private String ci;
    private Integer idCiTdocumento;
    private String ciTdocumento;
    private Integer idCiExpedido;
    private String ciExpedido;
    private String observacion;
    private Integer nroHijos;
    private Integer estadoPersona;
    
    /// PROPIEDADES DE ESTUDIANTE
    
    private String nroRegistro;
    private Integer anioIngresoUmsa;
    private Integer tituloBachiller;
    
    /// PROPIEDADES DE EST_CARRERAS
    
    private Long idCarreraEstudiante;
    private String carreraEstudiante;
    private Long idFacultadEstudiante;
    private String facultadEstudiante;
    private Integer anioIngCarrera;
    private Integer idTipoAlumno;
    private String tipoAlumno;
    private Integer idTipoIngreso;
    private String tipoIngreso;
    private String descripcionEstudianteCarrera;
    private Integer idTipoEscala = 1; ///PREGUNTAR
    private Date fecRegistro;
    private String usuario;
    private Short estadoEstudiante;
    
    private String idPersonaMatriculacion;
    
    private Integer registroServidor = 1;
    

    
    //PARA WS
    private String postgrado;
    private String estadoEst;
    private String fecNacimiento;
    private String idPaisOrigen;
    private String imagen;
    
    

    public Long getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(Long idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public Long getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Long idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdEcivil() {
        return idEcivil;
    }

    public void setIdEcivil(Integer idEcivil) {
        this.idEcivil = idEcivil;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public Integer getIdSexo() {
        return idSexo;
    }

    public void setIdSexo(Integer idSexo) {
        this.idSexo = idSexo;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getCasada() {
        return casada;
    }

    public void setCasada(String casada) {
        this.casada = casada;
    }

    public Date getNacFecha() {
        return nacFecha;
    }

    public void setNacFecha(Date nacFecha) {
        this.nacFecha = nacFecha;
    }

    public Integer getIdNacProvin() {
        return idNacProvin;
    }

    public void setIdNacProvin(Integer idNacProvin) {
        this.idNacProvin = idNacProvin;
    }

    public String getNacProvin() {
        return nacProvin;
    }

    public void setNacProvin(String nacProvin) {
        this.nacProvin = nacProvin;
    }

    public String getNacLocal() {
        return nacLocal;
    }

    public void setNacLocal(String nacLocal) {
        this.nacLocal = nacLocal;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public Integer getIdCiTdocumento() {
        return idCiTdocumento;
    }

    public void setIdCiTdocumento(Integer idCiTdocumento) {
        this.idCiTdocumento = idCiTdocumento;
    }

    public String getCiTdocumento() {
        return ciTdocumento;
    }

    public void setCiTdocumento(String ciTdocumento) {
        this.ciTdocumento = ciTdocumento;
    }

    public Integer getIdCiExpedido() {
        return idCiExpedido;
    }

    public void setIdCiExpedido(Integer idCiExpedido) {
        this.idCiExpedido = idCiExpedido;
    }

    public String getCiExpedido() {
        return ciExpedido;
    }

    public void setCiExpedido(String ciExpedido) {
        this.ciExpedido = ciExpedido;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getNroHijos() {
        return nroHijos;
    }

    public void setNroHijos(Integer nroHijos) {
        this.nroHijos = nroHijos;
    }

    public Integer getEstadoPersona() {
        return estadoPersona;
    }

    public void setEstadoPersona(Integer estadoPersona) {
        this.estadoPersona = estadoPersona;
    }

    public String getNroRegistro() {
        return nroRegistro;
    }

    public void setNroRegistro(String nroRegistro) {
        this.nroRegistro = nroRegistro;
    }

    public Integer getAnioIngresoUmsa() {
        return anioIngresoUmsa;
    }

    public void setAnioIngresoUmsa(Integer anioIngresoUmsa) {
        this.anioIngresoUmsa = anioIngresoUmsa;
    }

    public Integer getTituloBachiller() {
        return tituloBachiller;
    }

    public void setTituloBachiller(Integer tituloBachiller) {
        this.tituloBachiller = tituloBachiller;
    }

    public Long getIdCarreraEstudiante() {
        return idCarreraEstudiante;
    }

    public void setIdCarreraEstudiante(Long idCarreraEstudiante) {
        this.idCarreraEstudiante = idCarreraEstudiante;
    }

    public String getCarreraEstudiante() {
        return carreraEstudiante;
    }

    public void setCarreraEstudiante(String carreraEstudiante) {
        this.carreraEstudiante = carreraEstudiante;
    }

    public String getFacultadEstudiante() {
        return facultadEstudiante;
    }

    public void setFacultadEstudiante(String facultadEstudiante) {
        this.facultadEstudiante = facultadEstudiante;
    }

    public Integer getAnioIngCarrera() {
        return anioIngCarrera;
    }

    public void setAnioIngCarrera(Integer anioIngCarrera) {
        this.anioIngCarrera = anioIngCarrera;
    }

    public Integer getIdTipoAlumno() {
        return idTipoAlumno;
    }

    public void setIdTipoAlumno(Integer idTipoAlumno) {
        this.idTipoAlumno = idTipoAlumno;
    }

    public Integer getIdTipoIngreso() {
        return idTipoIngreso;
    }

    public void setIdTipoIngreso(Integer idTipoIngreso) {
        this.idTipoIngreso = idTipoIngreso;
    }

    public String getTipoAlumno() {
        return tipoAlumno;
    }

    public void setTipoAlumno(String tipoAlumno) {
        this.tipoAlumno = tipoAlumno;
    }

    public String getTipoIngreso() {
        return tipoIngreso;
    }

    public void setTipoIngreso(String tipoIngreso) {
        this.tipoIngreso = tipoIngreso;
    }

    public String getDescripcionEstudianteCarrera() {
        return descripcionEstudianteCarrera;
    }

    public void setDescripcionEstudianteCarrera(String descripcionEstudianteCarrera) {
        this.descripcionEstudianteCarrera = descripcionEstudianteCarrera;
    }

    public Integer getIdTipoEscala() {
        return idTipoEscala;
    }

    public void setIdTipoEscala(Integer idTipoEscala) {
        this.idTipoEscala = idTipoEscala;
    }

    public Date getFecRegistro() {
        return fecRegistro;
    }

    public void setFecRegistro(Date fecRegistro) {
        this.fecRegistro = fecRegistro;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Short getEstadoEstudiante() {
        return estadoEstudiante;
    }

    public void setEstadoEstudiante(Short estadoEstudiante) {
        this.estadoEstudiante = estadoEstudiante;
    }


    public Long getIdFacultadEstudiante() {
        return idFacultadEstudiante;
    }

    public String getIdPersonaMatriculacion() {
        return idPersonaMatriculacion;
    }

    public void setIdPersonaMatriculacion(String idPersonaMatriculacion) {
        this.idPersonaMatriculacion = idPersonaMatriculacion;
    }
    
    public String getNombreCompleto()
    {
        return (getPaterno() != null ? getPaterno() : "") + " " + 
                (getMaterno() != null ? getMaterno() : "") + " " + 
                (getNombres() != null ? getNombres() : "") + " " + 
                (getCasada() != null ? getCasada() : "");
    }
    
    public void setNombreCompleto(String nombreCompleto)
    {
        
    }

 
    
    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WsEstudiante other = (WsEstudiante) obj;
        if (this.idEstudiante != other.idEstudiante && (this.idEstudiante == null || !this.idEstudiante.equals(other.idEstudiante))) {
            return false;
        }
        return true;
    }

    /**
     * @return the postgrado
     */
    public String getPostgrado() {
        return postgrado;
    }

    /**
     * @param postgrado the postgrado to set
     */
    public void setPostgrado(String postgrado) {
        this.postgrado = postgrado;
    }

    /**
     * @return the estadoEst
     */
    public String getEstadoEst() {
        return estadoEst;
    }

    /**
     * @param estadoEst the estadoEst to set
     */
    public void setEstadoEst(String estadoEst) {
        this.estadoEst = estadoEst;
    }

    /**
     * @return the fecNacimiento
     */
    public String getFecNacimiento() {
        return fecNacimiento;
    }

    /**
     * @param fecNacimiento the fecNacimiento to set
     */
    public void setFecNacimiento(String fecNacimiento) {
        this.fecNacimiento = fecNacimiento;
    }

    /**
     * @return the idPaisOrigen
     */
    public String getIdPaisOrigen() {
        return idPaisOrigen;
    }

    /**
     * @param idPaisOrigen the idPaisOrigen to set
     */
    public void setIdPaisOrigen(String idPaisOrigen) {
        this.idPaisOrigen = idPaisOrigen;
    }

    /**
     * @return the imagen
     */
    public String getImagen() {
        return imagen;
    }

    /**
     * @param imagen the imagen to set
     */
    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

}
