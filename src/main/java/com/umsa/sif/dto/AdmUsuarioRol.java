package com.umsa.sif.dto;

import java.util.Date;

public class AdmUsuarioRol {
	private Long idUsuario;
	private Long idRol;
	private Integer estado;
	private Date fecReg = new Date();
	private Long usuReg;
	private Date fecMod = new Date();
	private Long usuMod;
	
	private String rol;

	public AdmUsuarioRol() {
		super();
	}

	public AdmUsuarioRol(Long idUsuario, Long idRol, Integer estado, Long usuReg, Long usuMod) {
		super();
		this.idUsuario = idUsuario;
		this.idRol = idRol;
		this.estado = estado;
		this.usuReg = usuReg;
		this.usuMod = usuMod;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Long getIdRol() {
		return idRol;
	}

	public void setIdRol(Long idRol) {
		this.idRol = idRol;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Date getFecReg() {
		return fecReg;
	}

	public void setFecReg(Date fecReg) {
		this.fecReg = new Date();
	}

	public Long getUsuReg() {
		return usuReg;
	}

	public void setUsuReg(Long usuReg) {
		this.usuReg = usuReg;
	}

	public Date getFecMod() {
		return fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = new Date();
	}

	public Long getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(Long usuMod) {
		this.usuMod = usuMod;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}
	
	

}
