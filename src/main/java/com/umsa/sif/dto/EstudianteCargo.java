package com.umsa.sif.dto;

import java.util.Date;

public class EstudianteCargo {
	private Long idEstudianteCargo;
	private Long idEstudiante;
	private Long idCargoEstudiantil;
	private Long idTipoCargoNro;
	private Long idTipoCargoNivel;
	private Date fecDesde;
	private Date fecHasta;
	private String frente;
	private String observacion;
	private Date fecReg = new Date();
	private Long usuReg;
	private Date fecMod = new Date();
	private Long usuMod;
	private Integer estado;
	private Long idUnidadAutoridad;
	private String unidadAutoridad;

	private Long idPersona;
	private Long idUnidad;
	private String ru;
	private Integer idSexo;
	private String apPaterno;
	private String apMaterno;
	private String apCasada;
	private String nombres;
	private String nroCi;
	private String extCi;
	private String lugCi;
	private Date fecNacimiento;
	private String dirFoto;
	private String direccion;
	private String telefonos;
	private String correos;
	private String unidadAcademica;
	private String unidadPadr;

	private String nombreCompleto;
	private String estamento;
	private Integer idEstamento;

	private String cargo;
	private Integer idTipoCargoEstudiantil;
	private String tipoCargo;
	private String tipoCargoNivel;
	private String tipoCargoNro;
	private Long idUnidadAutoridadPadre;
	private String unidadAutoridadPadre;

	public String getUnidadAutoridadPadre() {
		return unidadAutoridadPadre;
	}

	public void setUnidadAutoridadPadre(String unidadAutoridadPadre) {
		this.unidadAutoridadPadre = unidadAutoridadPadre;
	}

	public Long getIdUnidadAutoridadPadre() {
		return idUnidadAutoridadPadre;
	}

	public void setIdUnidadAutoridadPadre(Long idUnidadAutoridadPadre) {
		this.idUnidadAutoridadPadre = idUnidadAutoridadPadre;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public Integer getIdTipoCargoEstudiantil() {
		return idTipoCargoEstudiantil;
	}

	public void setIdTipoCargoEstudiantil(Integer idTipoCargoEstudiantil) {
		this.idTipoCargoEstudiantil = idTipoCargoEstudiantil;
	}

	public String getTipoCargo() {
		return tipoCargo;
	}

	public void setTipoCargo(String tipoCargo) {
		this.tipoCargo = tipoCargo;
	}

	public String getTipoCargoNivel() {
		return tipoCargoNivel;
	}

	public void setTipoCargoNivel(String tipoCargoNivel) {
		this.tipoCargoNivel = tipoCargoNivel;
	}

	public String getTipoCargoNro() {
		return tipoCargoNro;
	}

	public void setTipoCargoNro(String tipoCargoNro) {
		this.tipoCargoNro = tipoCargoNro;
	}

	public String getEstamento() {
		return estamento;
	}

	public void setEstamento(String estamento) {
		this.estamento = estamento;
	}

	public Integer getIdEstamento() {
		return idEstamento;
	}

	public void setIdEstamento(Integer idEstamento) {
		this.idEstamento = idEstamento;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public Long getIdEstudianteCargo() {
		return idEstudianteCargo;
	}

	public void setIdEstudianteCargo(Long idEstudianteCargo) {
		this.idEstudianteCargo = idEstudianteCargo;
	}

	public Long getIdEstudiante() {
		return idEstudiante;
	}

	public void setIdEstudiante(Long idEstudiante) {
		this.idEstudiante = idEstudiante;
	}

	public Long getIdCargoEstudiantil() {
		return idCargoEstudiantil;
	}

	public void setIdCargoEstudiantil(Long idCargoEstudiantil) {
		this.idCargoEstudiantil = idCargoEstudiantil;
	}

	public Long getIdTipoCargoNro() {
		return idTipoCargoNro;
	}

	public void setIdTipoCargoNro(Long idTipoCargoNro) {
		this.idTipoCargoNro = idTipoCargoNro;
	}

	public Long getIdTipoCargoNivel() {
		return idTipoCargoNivel;
	}

	public void setIdTipoCargoNivel(Long idTipoCargoNivel) {
		this.idTipoCargoNivel = idTipoCargoNivel;
	}

	public Date getFecDesde() {
		return fecDesde;
	}

	public void setFecDesde(Date fecDesde) {
		this.fecDesde = fecDesde;
	}

	public Date getFecHasta() {
		return fecHasta;
	}

	public void setFecHasta(Date fecHasta) {
		this.fecHasta = fecHasta;
	}

	public String getFrente() {
		return frente;
	}

	public void setFrente(String frente) {
		this.frente = frente;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Date getFecReg() {
		return fecReg;
	}

	public void setFecReg(Date fecReg) {
		this.fecReg = new Date();
	}

	public Long getUsuReg() {
		return usuReg;
	}

	public void setUsuReg(Long usuReg) {
		this.usuReg = usuReg;
	}

	public Date getFecMod() {
		return fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = new Date();
	}

	public Long getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(Long usuMod) {
		this.usuMod = usuMod;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public Long getIdUnidad() {
		return idUnidad;
	}

	public void setIdUnidad(Long idUnidad) {
		this.idUnidad = idUnidad;
	}

	public String getRu() {
		return ru;
	}

	public void setRu(String ru) {
		this.ru = ru;
	}

	public Integer getIdSexo() {
		return idSexo;
	}

	public void setIdSexo(Integer idSexo) {
		this.idSexo = idSexo;
	}

	public String getApPaterno() {
		return apPaterno;
	}

	public void setApPaterno(String apPaterno) {
		this.apPaterno = apPaterno;
	}

	public String getApMaterno() {
		return apMaterno;
	}

	public void setApMaterno(String apMaterno) {
		this.apMaterno = apMaterno;
	}

	public String getApCasada() {
		return apCasada;
	}

	public void setApCasada(String apCasada) {
		this.apCasada = apCasada;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getNroCi() {
		return nroCi;
	}

	public void setNroCi(String nroCi) {
		this.nroCi = nroCi;
	}

	public String getExtCi() {
		return extCi;
	}

	public void setExtCi(String extCi) {
		this.extCi = extCi;
	}

	public String getLugCi() {
		return lugCi;
	}

	public void setLugCi(String lugCi) {
		this.lugCi = lugCi;
	}

	public Date getFecNacimiento() {
		return fecNacimiento;
	}

	public void setFecNacimiento(Date fecNacimiento) {
		this.fecNacimiento = fecNacimiento;
	}

	public String getDirFoto() {
		return dirFoto;
	}

	public void setDirFoto(String dirFoto) {
		this.dirFoto = dirFoto;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefonos() {
		return telefonos;
	}

	public void setTelefonos(String telefonos) {
		this.telefonos = telefonos;
	}

	public String getCorreos() {
		return correos;
	}

	public void setCorreos(String correos) {
		this.correos = correos;
	}

	public String getUnidadAcademica() {
		return unidadAcademica;
	}

	public void setUnidadAcademica(String unidadAcademica) {
		this.unidadAcademica = unidadAcademica;
	}

	public String getUnidadPadr() {
		return unidadPadr;
	}

	public void setUnidadPadr(String unidadPadr) {
		this.unidadPadr = unidadPadr;
	}

	public Long getIdUnidadAutoridad() {
		return idUnidadAutoridad;
	}

	public void setIdUnidadAutoridad(Long idUnidadAutoridad) {
		this.idUnidadAutoridad = idUnidadAutoridad;
	}

	public String getUnidadAutoridad() {
		return unidadAutoridad;
	}

	public void setUnidadAutoridad(String unidadAutoridad) {
		this.unidadAutoridad = unidadAutoridad;
	}

}
