package com.umsa.sif.dto;

public class CargoEstudiantil {
	private Long idCargoEstudiantil;
	private Long idTipoCargoEstudiantil;
	private String cargo;
	private String cargoAbreviatura;
	private Integer estado;
	private Integer tipoEstamento;
	private String tipoCargo;
	private String tipoCargoAbreviatura;
	
	public Integer getTipoEstamento() {
		return tipoEstamento;
	}
	public void setTipoEstamento(Integer tipoEstamento) {
		this.tipoEstamento = tipoEstamento;
	}
	public Long getIdCargoEstudiantil() {
		return idCargoEstudiantil;
	}
	public void setIdCargoEstudiantil(Long idCargoEstudiantil) {
		this.idCargoEstudiantil = idCargoEstudiantil;
	}
	public Long getIdTipoCargoEstudiantil() {
		return idTipoCargoEstudiantil;
	}
	public void setIdTipoCargoEstudiantil(Long idTipoCargoEstudiantil) {
		this.idTipoCargoEstudiantil = idTipoCargoEstudiantil;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public String getCargoAbreviatura() {
		return cargoAbreviatura;
	}
	public void setCargoAbreviatura(String cargoAbreviatura) {
		this.cargoAbreviatura = cargoAbreviatura;
	}
	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	public String getTipoCargo() {
		return tipoCargo;
	}
	public void setTipoCargo(String tipoCargo) {
		this.tipoCargo = tipoCargo;
	}
	public String getTipoCargoAbreviatura() {
		return tipoCargoAbreviatura;
	}
	public void setTipoCargoAbreviatura(String tipoCargoAbreviatura) {
		this.tipoCargoAbreviatura = tipoCargoAbreviatura;
	}
	
	
	

}
