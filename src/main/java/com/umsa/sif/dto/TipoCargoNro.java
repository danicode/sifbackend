package com.umsa.sif.dto;

public class TipoCargoNro {
	private Long idTipoCargoNro;
	private String tipoCargoNro;
	private Integer estado;

	public Long getIdTipoCargoNro() {
		return idTipoCargoNro;
	}

	public void setIdTipoCargoNro(Long idTipoCargoNro) {
		this.idTipoCargoNro = idTipoCargoNro;
	}

	public String getTipoCargoNro() {
		return tipoCargoNro;
	}

	public void setTipoCargoNro(String tipoCargoNro) {
		this.tipoCargoNro = tipoCargoNro;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

}
