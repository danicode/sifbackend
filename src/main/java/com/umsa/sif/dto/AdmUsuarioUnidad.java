package com.umsa.sif.dto;

import java.util.Date;

public class AdmUsuarioUnidad {
	private Long idUsuario;
	private Long idUnidad;
	private Integer estado;
	private Date fecReg = new Date();
	private Long usuReg;
	private Date fecMod = new Date();
	private Long usuMod;

	private String unidadAcademicaPadre;
	private String unidadAcademica;
	private Long idUnidadPadre;

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Long getIdUnidad() {
		return idUnidad;
	}

	public void setIdUnidad(Long idUnidad) {
		this.idUnidad = idUnidad;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Date getFecReg() {
		return fecReg;
	}

	public void setFecReg(Date fecReg) {
		this.fecReg = new Date();
	}

	public Long getUsuReg() {
		return usuReg;
	}

	public void setUsuReg(Long usuReg) {
		this.usuReg = usuReg;
	}

	public Date getFecMod() {
		return fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = new Date();
	}

	public Long getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(Long usuMod) {
		this.usuMod = usuMod;
	}

	public String getUnidadAcademicaPadre() {
		return unidadAcademicaPadre;
	}

	public void setUnidadAcademicaPadre(String unidadAcademicaPadre) {
		this.unidadAcademicaPadre = unidadAcademicaPadre;
	}

	public String getUnidadAcademica() {
		return unidadAcademica;
	}

	public void setUnidadAcademica(String unidadAcademica) {
		this.unidadAcademica = unidadAcademica;
	}

	public Long getIdUnidadPadre() {
		return idUnidadPadre;
	}

	public void setIdUnidadPadre(Long idUnidadPadre) {
		this.idUnidadPadre = idUnidadPadre;
	}

}
