/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umsa.sif.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class WsPersona implements Serializable {

    private Long idPersona;
    private String idPersonaDoc;
    private String paterno = "";
    private String materno = "";
    private String casada = "";
    private String nombres = "";
    private String nombreCompleto;
    private Date fechaNacimiento;
    private Long idProvincia;
    private String provincia;
    private String localNacimiento;
    private Long idTipoDocumento;
    private String tipoDocumento;
    private String ci = "";
    private Long ciExpedido;
    private String departamento;
    private String expedido;
    private Long idEstadoCivil;
    private String estadoCivil = "";
    private Long idSexo;
    private String sexo = "";
    private String correo;
    private String telefono;
    private String celular;
    private String direccionDomicilio;
    private String direccionTrabajo;
    private String observacion;
    private String personaObservacion;
    private Integer estado;
    private Date fecRegistro;
    private Date fecModificacion;
    private Long idUsuarioModificacion;
    private Long idUsuarioRegistro;
    private String nroLibretaMilitar;
    private String fotografia;
    private String vigenciaCi;
    private Long idGrupoSanguineo;
    private Long idPais;
    private Long idDepartamento;
    private List<AdmUsuario> admUsuarioList;
    //datos caso persona
    private String cargo = "";
    private Integer nivel;
    private String unidad;
    private Integer item;
    private String descriDepartamento;
    private String descriPais;
    private String descriProvincia;
    private Integer estadoVerificacion;
    private Long idPersonaVerificar;
    private Integer tipo;
    private String edad;
    private Long idPaisExpedido;
    private Integer idCiTipoDocumento;
    private Integer idTipoPartida;
    private Integer idUsuario;
    private String usuario;
    private Integer idRol;
    private String rolDescri;
    private String correoPersonal;
    private String correoGrupo;
    private Integer pResultado;
    private String puestoActual;
    private String puestoinicio;
    private Integer tipomemo;
    private String tipomemoranda;
    private String nromemo;
    private Date fechaAplicacionMemoranda;
    private List<Persona> listaPadres;
    
    private Integer idTipo;
    private String tipoParentesco;
    private Integer edadAnios;
    private Integer edadAniosMin;
    private Integer idTipoParentesco;

    private String folio;
    private String partida;
    private String libro;
    private String nroCertificado;
    private String parentescoPadre;
    private String parentescoMadre;
    private String mes;
    private String anio;
    private String ciPostulante;
    private String descripcion;
    private String ciParentesco;
    private String nombreParentesco;
    
    private String denominacion;
    private String desCargo;
    private String codCh;
    private String desCategoria;
    private String facultad;    

    private String carrera;
    private String calificacion;
    
    private String anioEgreso;
    private String nro;
    private String temaProyecto;
    
//    private List<RhdWsDocente> listaDocentes;

    private String idEstudiante;
    private String nroTitulo;
    private String fecExpedicion;
    private String gradoAcademico;
        
//    public List<RhdWsDocente> getListaDocentes() {
//        return listaDocentes;
//    }

    public String getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(String idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public String getNroTitulo() {
        return nroTitulo;
    }

    public void setNroTitulo(String nroTitulo) {
        this.nroTitulo = nroTitulo;
    }

    public String getFecExpedicion() {
        return fecExpedicion;
    }

    public void setFecExpedicion(String fecExpedicion) {
        this.fecExpedicion = fecExpedicion;
    }

    public String getGradoAcademico() {
        return gradoAcademico;
    }

    public void setGradoAcademico(String gradoAcademico) {
        this.gradoAcademico = gradoAcademico;
    }

//    public void setListaDocentes(List<RhdWsDocente> listaDocentes) {
//        this.listaDocentes = listaDocentes;
//    }
//    
//    

    public String getCiPostulante() {
        return ciPostulante;
    }

    public void setCiPostulante(String ciPostulante) {
        this.ciPostulante = ciPostulante;
    }
    
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCiParentesco() {
        return ciParentesco;
    }

    public void setCiParentesco(String ciParentesco) {
        this.ciParentesco = ciParentesco;
    }

    public String getNombreParentesco() {
        return nombreParentesco;
    }

    public void setNombreParentesco(String nombreParentesco) {
        this.nombreParentesco = nombreParentesco;
    }
    
    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getPartida() {
        return partida;
    }

    public void setPartida(String partida) {
        this.partida = partida;
    }

    public String getLibro() {
        return libro;
    }

    public void setLibro(String libro) {
        this.libro = libro;
    }

    public String getNroCertificado() {
        return nroCertificado;
    }

    public void setNroCertificado(String nroCertificado) {
        this.nroCertificado = nroCertificado;
    }

    public String getParentescoPadre() {
        return parentescoPadre;
    }

    public void setParentescoPadre(String parentescoPadre) {
        this.parentescoPadre = parentescoPadre;
    }

    public String getParentescoMadre() {
        return parentescoMadre;
    }

    public void setParentescoMadre(String parentescoMadre) {
        this.parentescoMadre = parentescoMadre;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }
    
    public Integer getpResultado() {
        return pResultado;
    }

    public void setpResultado(Integer pResultado) {
        this.pResultado = pResultado;
    }
    private String clave;
    private String recordatorio;

    public String getCorreoPersonal() {
        return correoPersonal;
    }

    public void setCorreoPersonal(String correoPersonal) {
        this.correoPersonal = correoPersonal;
    }

    public String getCorreoGrupo() {
        return correoGrupo;
    }

    public void setCorreoGrupo(String correoGrupo) {
        this.correoGrupo = correoGrupo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getRecordatorio() {
        return recordatorio;
    }

    public void setRecordatorio(String recordatorio) {
        this.recordatorio = recordatorio;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public Long getIdPersonaVerificar() {
        return idPersonaVerificar;
    }

    public void setIdPersonaVerificar(Long idPersonaVerificar) {
        this.idPersonaVerificar = idPersonaVerificar;
    }

    public Integer getEstadoVerificacion() {
        return estadoVerificacion;
    }

    public void setEstadoVerificacion(Integer estadoVerificacion) {
        this.estadoVerificacion = estadoVerificacion;
    }

    public WsPersona() {
    }

    public Long getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Long idPersona) {
        this.idPersona = idPersona;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public String getCasada() {
        return casada;
    }

    public void setCasada(String casada) {
        this.casada = casada;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

  
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }


    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDateFechaNacimientoFormat() {
        if (fechaNacimiento == null) {
            return "";
        } else {
            return new SimpleDateFormat("dd-MM-yyy").format(fechaNacimiento);
        }
    }

    public String getLocalNacimiento() {
        return localNacimiento;
    }

    public void setLocalNacimiento(String localNacimiento) {
        this.localNacimiento = localNacimiento;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getDireccionDomicilio() {
        return direccionDomicilio;
    }

    public void setDireccionDomicilio(String direccionDomicilio) {
        this.direccionDomicilio = direccionDomicilio;
    }

    public String getDireccionTrabajo() {
        return direccionTrabajo;
    }

    public void setDireccionTrabajo(String direccionTrabajo) {
        this.direccionTrabajo = direccionTrabajo;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Date getFecRegistro() {
        return fecRegistro;
    }

    public void setFecRegistro(Date fecRegistro) {
        this.fecRegistro = fecRegistro;
    }

    public Date getFecModificacion() {
        return fecModificacion;
    }

    public void setFecModificacion(Date fecModificacion) {
        this.fecModificacion = fecModificacion;
    }

    public Long getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Long idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public Long getIdSexo() {
        return idSexo;
    }

    public void setIdSexo(Long idSexo) {
        this.idSexo = idSexo;
    }

    public Long getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(Long idProvincia) {
        this.idProvincia = idProvincia;
    }

    public Long getIdEstadoCivil() {
        return idEstadoCivil;
    }

    public void setIdEstadoCivil(Long idEstadoCivil) {
        this.idEstadoCivil = idEstadoCivil;
    }

    public Long getCiExpedido() {
        return ciExpedido;
    }

    public void setCiExpedido(Long ciExpedido) {
        this.ciExpedido = ciExpedido;
    }

    public Long getIdUsuarioModificacion() {
        return idUsuarioModificacion;
    }

    public void setIdUsuarioModificacion(Long idUsuarioModificacion) {
        this.idUsuarioModificacion = idUsuarioModificacion;
    }

    public Long getIdUsuarioRegistro() {
        return idUsuarioRegistro;
    }

    public void setIdUsuarioRegistro(Long idUsuarioRegistro) {
        this.idUsuarioRegistro = idUsuarioRegistro;
    }

    public List<AdmUsuario> getAdmUsuarioList() {
        return admUsuarioList;
    }

    public void setAdmUsuarioList(List<AdmUsuario> admUsuarioList) {
        this.admUsuarioList = admUsuarioList;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo != null ? cargo : "";
    }

    public Long getIdPais() {
        return idPais;
    }

    public void setIdPais(Long idPais) {
        this.idPais = idPais;
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad != null ? unidad : "";
    }

    public Integer getItem() {
        return item;
    }

    public void setItem(Integer item) {
        this.item = item;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public Long getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Long idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public String getDescriDepartamento() {
        return descriDepartamento;
    }

    public void setDescriDepartamento(String descriDepartamento) {
        this.descriDepartamento = descriDepartamento;
    }

    public String getDescriPais() {
        return descriPais;
    }

    public void setDescriPais(String descriPais) {
        this.descriPais = descriPais;
    }

    public String getDescriProvincia() {
        return descriProvincia;
    }

    public void setDescriProvincia(String descriProvincia) {
        this.descriProvincia = descriProvincia;
    }

    public String getPersonaObservacion() {
        return personaObservacion;
    }

    public void setPersonaObservacion(String personaObservacion) {
        this.personaObservacion = personaObservacion;
    }

    public String getExpedido() {
        return expedido;
    }

    public void setExpedido(String expedido) {
        this.expedido = expedido;
    }

    public String getNroLibretaMilitar() {
        return nroLibretaMilitar;
    }

    public void setNroLibretaMilitar(String nroLibretaMilitar) {
        this.nroLibretaMilitar = nroLibretaMilitar;
    }

    public String getFotografia() {
        return fotografia;
    }

    public void setFotografia(String fotografia) {
        this.fotografia = fotografia;
    }

    public String getVigenciaCi() {
        return vigenciaCi;
    }

    public void setVigenciaCi(String vigenciaCi) {
        this.vigenciaCi = vigenciaCi;
    }

    public String getDateNacimientoFormat() {
        if (fechaNacimiento == null) {
            return "";
        } else {
            return new SimpleDateFormat("dd-MM-yyy").format(fechaNacimiento);
        }
    }

    public Long getIdGrupoSanguineo() {
        return idGrupoSanguineo;
    }

    public void setIdGrupoSanguineo(Long idGrupoSanguineo) {
        this.idGrupoSanguineo = idGrupoSanguineo;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Long getIdPaisExpedido() {
        return idPaisExpedido;
    }

    public void setIdPaisExpedido(Long idPaisExpedido) {
        this.idPaisExpedido = idPaisExpedido;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    public String getRolDescri() {
        return rolDescri;
    }

    public void setRolDescri(String rolDescri) {
        this.rolDescri = rolDescri;
    }

    public Integer getIdCiTipoDocumento() {
        return idCiTipoDocumento;
    }

    public void setIdCiTipoDocumento(Integer idCiTipoDocumento) {
        this.idCiTipoDocumento = idCiTipoDocumento;
    }

    public Integer getIdTipoPartida() {
        return idTipoPartida;
    }

    public void setIdTipoPartida(Integer idTipoPartida) {
        this.idTipoPartida = idTipoPartida;
    }

    public String getPuestoActual() {
        return puestoActual;
    }

    public void setPuestoActual(String puestoActual) {
        this.puestoActual = puestoActual;
    }


    public Date getFechaAplicacionMemoranda() {
        return fechaAplicacionMemoranda;
    }

    public void setFechaAplicacionMemoranda(Date fechaAplicacionMemoranda) {
        this.fechaAplicacionMemoranda = fechaAplicacionMemoranda;
    }

    public String getPuestoinicio() {
        return puestoinicio;
    }

    public void setPuestoinicio(String puestoinicio) {
        this.puestoinicio = puestoinicio;
    }

    public Integer getTipomemo() {
        return tipomemo;
    }

    public void setTipomemo(Integer tipomemo) {
        this.tipomemo = tipomemo;
    }

    public String getTipomemoranda() {
        return tipomemoranda;
    }

    public void setTipomemoranda(String tipomemoranda) {
        this.tipomemoranda = tipomemoranda;
    }

    public String getNromemo() {
        return nromemo;
    }

    public void setNromemo(String nromemo) {
        this.nromemo = nromemo;
    }

    public Integer getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(Integer idTipo) {
        this.idTipo = idTipo;
    }

    public String getTipoParentesco() {
        return tipoParentesco;
    }

    public void setTipoParentesco(String tipoParentesco) {
        this.tipoParentesco = tipoParentesco;
    }

    public List<Persona> getListaPadres() {
        return listaPadres;
    }

    public void setListaPadres(List<Persona> listaPadres) {
        this.listaPadres = listaPadres;
    }

    public Integer getEdadAnios() {
        return edadAnios;
    }

    public void setEdadAnios(Integer edadAnios) {
        this.edadAnios = edadAnios;
    }

    public Integer getEdadAniosMin() {
        return edadAniosMin;
    }

    public void setEdadAniosMin(Integer edadAniosMin) {
        this.edadAniosMin = edadAniosMin;
    }

    public Integer getIdTipoParentesco() {
        return idTipoParentesco;
    }

    public void setIdTipoParentesco(Integer idTipoParentesco) {
        this.idTipoParentesco = idTipoParentesco;
    }

    public String getIdPersonaDoc() {
        return idPersonaDoc;
    }

    public void setIdPersonaDoc(String idPersonaDoc) {
        this.idPersonaDoc = idPersonaDoc;
    }

    /**
     * @return the denominacion
     */
    public String getDenominacion() {
        return denominacion;
    }

    /**
     * @param denominacion the denominacion to set
     */
    public void setDenominacion(String denominacion) {
        this.denominacion = denominacion;
    }

    /**
     * @return the desCargo
     */
    public String getDesCargo() {
        return desCargo;
    }

    /**
     * @param desCargo the desCargo to set
     */
    public void setDesCargo(String desCargo) {
        this.desCargo = desCargo;
    }

    /**
     * @return the codCh
     */
    public String getCodCh() {
        return codCh;
    }

    /**
     * @param codCh the codCh to set
     */
    public void setCodCh(String codCh) {
        this.codCh = codCh;
    }

    /**
     * @return the desCategoria
     */
    public String getDesCategoria() {
        return desCategoria;
    }

    /**
     * @param desCategoria the desCategoria to set
     */
    public void setDesCategoria(String desCategoria) {
        this.desCategoria = desCategoria;
    }

    /**
     * @return the facultad
     */
    public String getFacultad() {
        return facultad;
    }

    /**
     * @param facultad the facultad to set
     */
    public void setFacultad(String facultad) {
        this.facultad = facultad;
    }

    /**
     * @return the carrera
     */
    public String getCarrera() {
        return carrera;
    }

    /**
     * @param carrera the carrera to set
     */
    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    /**
     * @return the calificacion
     */
    public String getCalificacion() {
        return calificacion;
    }

    /**
     * @param calificacion the calificacion to set
     */
    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    /**
     * @return the anioEgreso
     */
    public String getAnioEgreso() {
        return anioEgreso;
    }

    /**
     * @param anioEgreso the anioEgreso to set
     */
    public void setAnioEgreso(String anioEgreso) {
        this.anioEgreso = anioEgreso;
    }

    /**
     * @return the nro
     */
    public String getNro() {
        return nro;
    }

    /**
     * @param nro the nro to set
     */
    public void setNro(String nro) {
        this.nro = nro;
    }

    /**
     * @return the temaProyecto
     */
    public String getTemaProyecto() {
        return temaProyecto;
    }

    /**
     * @param temaProyecto the temaProyecto to set
     */
    public void setTemaProyecto(String temaProyecto) {
        this.temaProyecto = temaProyecto;
    }
    
}
