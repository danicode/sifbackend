package com.umsa.sif.dto.crit;

import java.util.Date;

public class CritEstudiante {
	private Long idEstudiante;
	private Long idPersona;
	private Long idUnidad;
	private String ru;
	private Integer estado;
	private Date fecReg = new Date();
	private Long usuReg;
	private Date fecMod = new Date();
	private Long usuMod;
	private Integer idSexo;
	private String apPaterno;
	private String apMaterno;
	private String apCasada;
	private String nombres;
	private String nroCi;
	private String extCi;
	private String lugCi;
	private Date fecNacimiento;
	private String dirFoto;
	private String direccion;
	private String telefonos;
	private String correos;
	private String unidadAcademica;
	private String unidadPadre;

	public CritEstudiante() {
		super();
	}

	public CritEstudiante(Long idEstudiante, Integer estado) {
		super();
		this.idEstudiante = idEstudiante;
		this.estado = estado;
	}

	public CritEstudiante(String ru, Integer estado) {
		super();
		this.ru = ru;
		this.estado = estado;
	}

	public Long getIdEstudiante() {
		return idEstudiante;
	}

	public void setIdEstudiante(Long idEstudiante) {
		this.idEstudiante = idEstudiante;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public Long getIdUnidad() {
		return idUnidad;
	}

	public void setIdUnidad(Long idUnidad) {
		this.idUnidad = idUnidad;
	}

	public String getRu() {
		return ru;
	}

	public void setRu(String ru) {
		this.ru = ru;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Date getFecReg() {
		return fecReg;
	}

	public void setFecReg(Date fecReg) {
		this.fecReg = new Date();
	}

	public Long getUsuReg() {
		return usuReg;
	}

	public void setUsuReg(Long usuReg) {
		this.usuReg = usuReg;
	}

	public Date getFecMod() {
		return fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = new Date();
	}

	public Long getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(Long usuMod) {
		this.usuMod = usuMod;
	}

	public Integer getIdSexo() {
		return idSexo;
	}

	public void setIdSexo(Integer idSexo) {
		this.idSexo = idSexo;
	}

	public String getApPaterno() {
		return apPaterno;
	}

	public void setApPaterno(String apPaterno) {
		this.apPaterno = apPaterno;
	}

	public String getApMaterno() {
		return apMaterno;
	}

	public void setApMaterno(String apMaterno) {
		this.apMaterno = apMaterno;
	}

	public String getApCasada() {
		return apCasada;
	}

	public void setApCasada(String apCasada) {
		this.apCasada = apCasada;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getNroCi() {
		return nroCi;
	}

	public void setNroCi(String nroCi) {
		this.nroCi = nroCi;
	}

	public String getExtCi() {
		return extCi;
	}

	public void setExtCi(String extCi) {
		this.extCi = extCi;
	}

	public String getLugCi() {
		return lugCi;
	}

	public void setLugCi(String lugCi) {
		this.lugCi = lugCi;
	}

	public Date getFecNacimiento() {
		return fecNacimiento;
	}

	public void setFecNacimiento(Date fecNacimiento) {
		this.fecNacimiento = fecNacimiento;
	}

	public String getDirFoto() {
		return dirFoto;
	}

	public void setDirFoto(String dirFoto) {
		this.dirFoto = dirFoto;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefonos() {
		return telefonos;
	}

	public void setTelefonos(String telefonos) {
		this.telefonos = telefonos;
	}

	public String getCorreos() {
		return correos;
	}

	public void setCorreos(String correos) {
		this.correos = correos;
	}

	public String getUnidadAcademica() {
		return unidadAcademica;
	}

	public void setUnidadAcademica(String unidadAcademica) {
		this.unidadAcademica = unidadAcademica;
	}

	public String getUnidadPadre() {
		return unidadPadre;
	}

	public void setUnidadPadre(String unidadPadre) {
		this.unidadPadre = unidadPadre;
	}

}
