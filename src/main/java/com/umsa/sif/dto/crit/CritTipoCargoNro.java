package com.umsa.sif.dto.crit;

public class CritTipoCargoNro {
	private Long idTipoCargoNro;
	private Long tipoCargoNro;
	private Integer estado;

	public Long getIdTipoCargoNro() {
		return idTipoCargoNro;
	}

	public void setIdTipoCargoNro(Long idTipoCargoNro) {
		this.idTipoCargoNro = idTipoCargoNro;
	}

	public Long getTipoCargoNro() {
		return tipoCargoNro;
	}

	public void setTipoCargoNro(Long tipoCargoNro) {
		this.tipoCargoNro = tipoCargoNro;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

}
