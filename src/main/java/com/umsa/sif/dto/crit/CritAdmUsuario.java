package com.umsa.sif.dto.crit;

import java.util.Date;

public class CritAdmUsuario {
	private Long idUsuario;
	private Long idPersona;
	private String usuario;
	private String clave;
	private Integer estado;
	private Date fecReg = new Date();
	private Long usuReg;
	private Date fecMod = new Date();
	private Long usuMod;

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Date getFecReg() {
		return fecReg;
	}

	public void setFecReg(Date fecReg) {
		this.fecReg = fecReg;
	}

	public Long getUsuReg() {
		return usuReg;
	}

	public void setUsuReg(Long usuReg) {
		this.usuReg = usuReg;
	}

	public Date getFecMod() {
		return fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public Long getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(Long usuMod) {
		this.usuMod = usuMod;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public CritAdmUsuario(String usuario) {
		super();
		this.usuario = usuario;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public CritAdmUsuario(Long idUsuario, Integer estado) {
		super();
		this.idUsuario = idUsuario;
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "AdmUsuario [idUsuario=" + idUsuario + ", usuario=" + usuario + ", clave=" + clave + "]";
	}

}
