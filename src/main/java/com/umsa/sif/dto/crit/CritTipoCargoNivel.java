package com.umsa.sif.dto.crit;

public class CritTipoCargoNivel {
	private Long idTipoCargoNivel;
	private Long tipoCargoNivel;
	private Integer estado;

	public Long getIdTipoCargoNivel() {
		return idTipoCargoNivel;
	}

	public void setIdTipoCargoNivel(Long idTipoCargoNivel) {
		this.idTipoCargoNivel = idTipoCargoNivel;
	}

	public Long getTipoCargoNivel() {
		return tipoCargoNivel;
	}

	public void setTipoCargoNivel(Long tipoCargoNivel) {
		this.tipoCargoNivel = tipoCargoNivel;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

}
