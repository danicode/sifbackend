package com.umsa.sif.dto;

public class TipoCargoNivel {
	private Long idTipoCargoNivel;
	private String tipoCargoNivel;
	private Integer estado;

	public Long getIdTipoCargoNivel() {
		return idTipoCargoNivel;
	}

	public void setIdTipoCargoNivel(Long idTipoCargoNivel) {
		this.idTipoCargoNivel = idTipoCargoNivel;
	}

	public String getTipoCargoNivel() {
		return tipoCargoNivel;
	}

	public void setTipoCargoNivel(String tipoCargoNivel) {
		this.tipoCargoNivel = tipoCargoNivel;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

}
