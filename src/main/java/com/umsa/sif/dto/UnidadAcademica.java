package com.umsa.sif.dto;

import java.util.Date;

public class UnidadAcademica {
	private Long idUnidad;
	private String unidadAcademica;
	private String idPrograma;
	private Long idUnidadPadre;
	private Integer nivel;
	
	private String unidadAcademicaPadre;
	private String codApePro;
	

	public String getCodApePro() {
		return codApePro;
	}

	public void setCodApePro(String codApePro) {
		this.codApePro = codApePro;
	}

	public Long getIdUnidad() {
		return idUnidad;
	}

	public void setIdUnidad(Long idUnidad) {
		this.idUnidad = idUnidad;
	}

	public String getUnidadAcademica() {
		return unidadAcademica;
	}

	public void setUnidadAcademica(String unidadAcademica) {
		this.unidadAcademica = unidadAcademica;
	}

	public String getIdPrograma() {
		return idPrograma;
	}

	public void setIdPrograma(String idPrograma) {
		this.idPrograma = idPrograma;
	}

	public Long getIdUnidadPadre() {
		return idUnidadPadre;
	}

	public void setIdUnidadPadre(Long idUnidadPadre) {
		this.idUnidadPadre = idUnidadPadre;
	}

	public Integer getNivel() {
		return nivel;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

	public String getUnidadAcademicaPadre() {
		return unidadAcademicaPadre;
	}

	public void setUnidadAcademicaPadre(String unidadAcademicaPadre) {
		this.unidadAcademicaPadre = unidadAcademicaPadre;
	}
	
	
}
