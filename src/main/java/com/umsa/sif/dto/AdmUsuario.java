package com.umsa.sif.dto;

import java.util.Date;

public class AdmUsuario {
	private Long idUsuario;
	private Long idPersona;
	private String usuario;
	private String clave;
	private Integer estado;
	private Date fecReg = new Date();
	private Long usuReg;
	private Date fecMod = new Date();
	private Long usuMod;
	
	private String nombreCompleto;
	
	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public AdmUsuario() {
		super();
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Date getFecReg() {
		return fecReg;
	}

	public void setFecReg(Date fecReg) {
		this.fecReg = new Date();
	}

	public Long getUsuReg() {
		return usuReg;
	}

	public void setUsuReg(Long usuReg) {
		this.usuReg = usuReg;
	}

	public Date getFecMod() {
		return fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = new Date();
	}

	public Long getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(Long usuMod) {
		this.usuMod = usuMod;
	}

	@Override
	public String toString() {
		return "AdmUsuario [idUsuario=" + idUsuario + ", usuario=" + usuario + ", clave=" + clave + "]";
	}

	public AdmUsuario(Long idUsuario, String usuario, String clave) {
		super();
		this.idUsuario = idUsuario;
		this.usuario = usuario;
		this.clave = clave;
	}
	public AdmUsuario(Long idUsuario, String usuario, Integer estado, String clave) {
		super();
		this.idUsuario = idUsuario;
		this.usuario = usuario;
		this.clave = clave;
		this.estado = estado;
	}

}
