package com.umsa.sif.controllers;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.umsa.sif.dto.AdmUsuario;
import com.umsa.sif.dto.AdmUsuarioRol;
import com.umsa.sif.dto.AdmUsuarioUnidad;
import com.umsa.sif.dto.Persona;
import com.umsa.sif.dto.crit.CritAdmUsuario;
import com.umsa.sif.dto.crit.CritAdmUsuarioRol;
import com.umsa.sif.dto.crit.CritAdmUsuarioUnidad;
import com.umsa.sif.dto.crit.CritPersona;
import com.umsa.sif.service.AdministracionService;
import com.umsa.sif.service.PersonaService;
import com.umsa.sif.service.WebService;
import com.umsa.sif.util.RegistrationResult;

@RestController
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminController {
	@Autowired
	PersonaService personaService;
	@Autowired
	AdministracionService administracionService;
	@Autowired
	PasswordEncoder passwordEncoder;
	@Autowired
	WebService webService;

	@PostMapping("api/personas/listarPersonas")
	public List<Persona> listarPersonas(@RequestBody CritPersona critPersona) throws URISyntaxException {
		critPersona.setEstado(1);
		System.out.println(critPersona.toString());
		return personaService.listarPersona(critPersona);
	}
	
	@GetMapping("api/personas/obtenerPersona/{idPersona}")
	public Persona obetenerPersona(@PathVariable Long idPersona) {
		CritPersona critPersona = new CritPersona(idPersona,1);
		List<Persona> listaPersonas = personaService.listarPersona(critPersona); 
		if(listaPersonas.size()==1) {
			return listaPersonas.get(0);
		} else {
			return new Persona();
		}
	}

	@PostMapping("api/personas/registrarPersona")
	public RegistrationResult registrarPersona(@RequestBody Persona persona) {
		persona.setEstado(1);
		return personaService.registrarPersona(persona);
	}
	
	@PostMapping("api/personas/eliminarPersona")
	public RegistrationResult eliminarPersona(@RequestBody Persona persona) {
		return personaService.eliminarPersona(persona);
	}
	
	@PostMapping("api/usuarios/listarUsuarios")
	public List<AdmUsuario> listarAdmUsuarios(@RequestBody CritAdmUsuario critAdmUsuario) {
		critAdmUsuario.setEstado(1);
		return administracionService.listarUsuarios(critAdmUsuario);
	}
	
	@GetMapping("api/usuarios/obtenerUsuario/{idAdmUsuario}")
	public AdmUsuario obetenerAdmUsuario(@PathVariable Long idAdmUsuario) {
		CritAdmUsuario critAdmUsuario = new CritAdmUsuario(idAdmUsuario,1);
		List<AdmUsuario> listaAdmUsuarios = administracionService.listarUsuarios(critAdmUsuario); 
		if(listaAdmUsuarios.size()==1) {
			AdmUsuario admUsuario = listaAdmUsuarios.get(0);
			admUsuario.setClave("");
			return admUsuario;
		} else {
			return new AdmUsuario();
		}
	}

	@PostMapping("api/usuarios/registrarUsuario")
	public RegistrationResult registrarAdmUsuario(@RequestBody AdmUsuario admUsuario) {
		admUsuario.setEstado(1);
		admUsuario.setClave(passwordEncoder.encode(admUsuario.getClave()));
		return administracionService.RegistrarUsuario(admUsuario);
	}
	
	@PostMapping("api/usuarios/eliminarUsuario")
	public RegistrationResult eliminarAdmUsuario(@RequestBody AdmUsuario admUsuario) {
		return administracionService.EliminarUsuario(admUsuario);
	}
	
	@PostMapping("api/usuarios/listarUsuarioRol")
	public List<AdmUsuarioRol> listarUsuarioRol(@RequestBody CritAdmUsuarioRol critAdmUsuarioRol) {
		critAdmUsuarioRol.setEstado(1);
		return administracionService.listarUsuarioRol(critAdmUsuarioRol);
	}
	
	@PostMapping("api/usuarios/registrarUsuarioRol")
	public RegistrationResult registrarUsuarioRol(@RequestBody AdmUsuarioRol admUsuarioRol) {
		admUsuarioRol.setEstado(1);
		return administracionService.registrarUsuarioRol(admUsuarioRol);
	}
	
	@PostMapping("api/usuarios/eliminarUsuarioRol")
	public RegistrationResult eliminarUsuarioRol(@RequestBody AdmUsuarioRol admUsuarioRol) {
		return administracionService.eliminarUsuarioRol(admUsuarioRol);
	}
	
	@PostMapping("api/usuarios/listarUsuarioUnidad")
	public List<AdmUsuarioUnidad> listarUsuarioRol(@RequestBody CritAdmUsuarioUnidad critAdmUsuarioUnidad) {
		critAdmUsuarioUnidad.setEstado(1);
		return administracionService.listarUsuarioUnidad(critAdmUsuarioUnidad);
	}
	
	@PostMapping("api/usuarios/registrarUsuarioUnidad")
	public RegistrationResult registrarUsuarioUnidad(@RequestBody AdmUsuarioUnidad admUsuarioUnidad) {
		admUsuarioUnidad.setEstado(1);
		return administracionService.registrarUsuarioUnidad(admUsuarioUnidad);
	}
	
	@PostMapping("api/usuarios/eliminarUsuarioUnidad")
	public RegistrationResult eliminarUsuarioUnidad(@RequestBody AdmUsuarioUnidad admUsuarioUnidad) {
		return administracionService.eliminarUsuarioUnidad(admUsuarioUnidad);
	}
}
