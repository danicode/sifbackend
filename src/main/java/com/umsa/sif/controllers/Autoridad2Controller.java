package com.umsa.sif.controllers;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.umsa.sif.dto.Docente;
import com.umsa.sif.dto.crit.CritDocente;
import com.umsa.sif.service.DocenteService;
import com.umsa.sif.service.WebService;

@RestController
@PreAuthorize("hasRole('ROLE_AUTORIDAD_2')")
public class Autoridad2Controller {
	
	@Autowired
	WebService webService;

	@PostMapping("api/autoridad2/docentes/listarDocentes2WebService")
	public List<Docente> listarDocentesWebService(@RequestBody CritDocente critDocente) throws ParseException, URISyntaxException {		
		return webService.listarDocentes2(critDocente);
	}

}
