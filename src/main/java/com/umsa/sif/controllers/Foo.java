package com.umsa.sif.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.umsa.sif.dto.AdmUsuario;
import com.umsa.sif.service.MainService;

//@CrossOrigin(origins = "*")
@RestController
public class Foo {
	@Autowired
	MainService mainService;
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("user")
	public List<AdmUsuario> listarUsuario() {
		return mainService.listarUsuario(null);
	}
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
//	@PostMapping("user")
//	public AdmUsuario post(@RequestBody AdmUsuario admUsuario) {
////		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
////		System.out.println();
//		admUsuario.toString();
////		return new AdmUsuario(2, "alejandro", "clave");
//	}

}
