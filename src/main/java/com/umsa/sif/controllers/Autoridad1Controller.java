package com.umsa.sif.controllers;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.umsa.sif.dto.AdmUsuarioUnidad;
import com.umsa.sif.dto.CargoEstudiantil;
import com.umsa.sif.dto.Docente;
import com.umsa.sif.dto.DocenteCargo;
import com.umsa.sif.dto.Estudiante;
import com.umsa.sif.dto.EstudianteCargo;
import com.umsa.sif.dto.TipoCargoNivel;
import com.umsa.sif.dto.TipoCargoNro;
import com.umsa.sif.dto.UnidadAcademica;
import com.umsa.sif.dto.crit.CritAdmUsuarioUnidad;
import com.umsa.sif.dto.crit.CritCargoEstudiantil;
import com.umsa.sif.dto.crit.CritDocente;
import com.umsa.sif.dto.crit.CritDocenteCargo;
import com.umsa.sif.dto.crit.CritEstudiante;
import com.umsa.sif.dto.crit.CritEstudianteCargo;
import com.umsa.sif.dto.crit.CritUnidadAcademica;
import com.umsa.sif.service.AdministracionService;
import com.umsa.sif.service.AutoridadService;
import com.umsa.sif.service.DocenteService;
import com.umsa.sif.service.EstudianteService;
import com.umsa.sif.service.PersonaService;
import com.umsa.sif.service.WebService;
import com.umsa.sif.util.RegistrationResult;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

@RestController
@PreAuthorize("hasRole('ROLE_AUTORIDAD_1')")
public class Autoridad1Controller {
	@Autowired
	PersonaService personaService;
	@Autowired
	AdministracionService administracionService;
	@Autowired
	AutoridadService autoridadService;
	@Autowired
	EstudianteService estudianteService;
	@Autowired
	DocenteService docenteService;
	@Autowired
	WebService webService;
	@Autowired
	private ResourceLoader resourceLoader;

	@PostMapping("api/autoridad/unidades/listarUsuarioUnidad")
	public List<AdmUsuarioUnidad> listarPersonas(@RequestBody CritAdmUsuarioUnidad critAdmUsuarioUnidad) {
		critAdmUsuarioUnidad.setEstado(1);
		return administracionService.listarUsuarioUnidad(critAdmUsuarioUnidad);
	}

	@GetMapping("api/autoridad/unidades/obtenerUnidades/{idUnidad}")
	public UnidadAcademica obtenerUnidades(@PathVariable Long idUnidad) {
		CritUnidadAcademica critUnidadAcademica = new CritUnidadAcademica(idUnidad);
		List<UnidadAcademica> listaUnidad = estudianteService.listarUnidadAcademica(critUnidadAcademica);
		if (listaUnidad.size() == 1) {
			return listaUnidad.get(0);
		} else {
			return new UnidadAcademica();

		}
	}

	@PostMapping("api/autoridad/unidades/listarUnidadesAcademicas")
	public List<UnidadAcademica> listarPersonas(@RequestBody CritUnidadAcademica critUnidadAcademica) {
		return estudianteService.listarUnidadAcademica(critUnidadAcademica);
	}

	@PostMapping("api/autoridad/estudianteCargo/listarTotalAutoridades")
	public List<EstudianteCargo> listarTotalAutoridades(@RequestBody CritEstudianteCargo critEstudianteCargo) {
		critEstudianteCargo.setEstado(1);
		return autoridadService.listarTotalAutoridades(critEstudianteCargo);
	}

	@PostMapping("api/autoridad/estudianteCargo/listarEstudianteCargo")
	public List<EstudianteCargo> listarEstudianteCargo(@RequestBody CritEstudianteCargo critEstudianteCargo) {
		critEstudianteCargo.setEstado(1);
		return autoridadService.listarEstudianteCargo(critEstudianteCargo);
	}

	@GetMapping("api/autoridad/estudianteCargo/obtenerEstudianteCargo/{idEstudianteCargo}")
	public EstudianteCargo obtenerEstudianteCargo(@PathVariable Long idEstudianteCargo) {
		CritEstudianteCargo critEstudianteCargo = new CritEstudianteCargo(idEstudianteCargo, 1);
		List<EstudianteCargo> listaEstudianteCargo = autoridadService.listarEstudianteCargo(critEstudianteCargo);
		if (listaEstudianteCargo.size() == 1) {
			EstudianteCargo estudianteCargo = listaEstudianteCargo.get(0);
			return estudianteCargo;
		} else {
			return new EstudianteCargo();
		}
	}

	@PostMapping("api/autoridad/estudianteCargo/registrarEstudianteCargo")
	public RegistrationResult registrarEstudianteCargo(@RequestBody EstudianteCargo estudianteCargo) {
		estudianteCargo.setEstado(1);
		return autoridadService.registrarEstudianteCargo(estudianteCargo);
	}

	@PostMapping("api/autoridad/estudianteCargo/eliminarEstudianteCargo")
	public RegistrationResult eliminarEstudianteCargo(@RequestBody EstudianteCargo estudianteCargo) {
		estudianteCargo.setEstado(1);
		return autoridadService.eliminarEstudianteCargo(estudianteCargo);
	}

	@PostMapping("api/autoridad/docenteCargo/listarDocenteCargo")
	public List<DocenteCargo> listarDocenteCargo(@RequestBody CritDocenteCargo critDocenteCargo) {
		critDocenteCargo.setEstado(1);
		return autoridadService.listarDocenteCargo(critDocenteCargo);
	}

	@GetMapping("api/autoridad/docenteCargo/obtenerDocenteCargo/{idDocenteCargo}")
	public DocenteCargo obtenerDocenteCargo(@PathVariable Long idDocenteCargo) {
		CritDocenteCargo critDocenteCargo = new CritDocenteCargo(idDocenteCargo, 1);
		List<DocenteCargo> listaDocenteCargo = autoridadService.listarDocenteCargo(critDocenteCargo);
		if (listaDocenteCargo.size() == 1) {
			DocenteCargo docenteCargo = listaDocenteCargo.get(0);
			return docenteCargo;
		} else {
			return new DocenteCargo();
		}
	}

	@PostMapping("api/autoridad/docenteCargo/registrarDocenteCargo")
	public RegistrationResult registrarDocenteCargo(@RequestBody DocenteCargo docenteCargo) {
		docenteCargo.setEstado(1);
		return autoridadService.registrarDocenteCargo(docenteCargo);
	}

	@PostMapping("api/autoridad/docenteCargo/eliminarDocenteCargo")
	public RegistrationResult eliminarDocenteCargo(@RequestBody DocenteCargo docenteCargo) {
		docenteCargo.setEstado(1);
		return autoridadService.eliminarDocenteCargo(docenteCargo);
	}

	@GetMapping("api/autoridad/estudiantes/listarEstudiantesMatriculacion/{ci}/{idUsuario}")
	public List<Estudiante> listarEstudiantesMatriculacion(@PathVariable String ci, @PathVariable Long idUsuario) throws ParseException {
		webService.registrarEstudiantePorCi(ci, idUsuario);
		CritEstudiante critEstudiante = new CritEstudiante();
		critEstudiante.setNroCi(ci);
		List<Estudiante> listaEstudiante = estudianteService.listarEstudiante(critEstudiante);
		return listaEstudiante;
	}

	// Listar Docentes WebService
	@GetMapping("api/autoridad/docentes/listarDocentesWebService/{ci}/{idUsuario}")
	public List<Docente> listarDocentesWebService(@PathVariable String ci, @PathVariable Long idUsuario) throws ParseException, URISyntaxException {
		webService.registrarDocentePorCi(ci, idUsuario);
		CritDocente critDocente = new CritDocente();
		critDocente.setNroCi(ci);
		critDocente.setEstado(1);
		List<Docente> listaDocente = docenteService.listarDocente(critDocente);
		return listaDocente;
	}

	@PostMapping("api/autoridad/listarCargoEstudiantil")
	public List<CargoEstudiantil> listarCargoEstudiantil(@RequestBody CritCargoEstudiantil critCargoEstudiantil) {
		return autoridadService.listarCargoEstudiantil(critCargoEstudiantil);
	}

	@GetMapping("api/autoridad/listarTipoCargoNivel")
	public List<TipoCargoNivel> listarTipoCargoNivel() {
		System.out.println("listarTipoCargoNivel");
		return autoridadService.listarTipoCargoNivel();
	}

	@GetMapping("api/autoridad/listarTipoCargoNro")
	public List<TipoCargoNro> listarTipoCargoNro() {
		System.out.println("listarTipoCargoNro");
		return autoridadService.listarTipoCargoNro();
	}

	@PostMapping("api/autoridad/estudiantes/listarEstudiantes")
	public List<Estudiante> listarEstudiantes(@RequestBody CritEstudiante critEstudiante) {
		critEstudiante.setEstado(1);
		return estudianteService.listarEstudiante(critEstudiante);
	}

	@GetMapping("api/autoridad/estudiantes/obtenerEstudiante/{idEstudiante}")
	public Estudiante obtenerEstudiante(@PathVariable Long idEstudiante) {
		CritEstudiante critEstudiante = new CritEstudiante(idEstudiante, 1);
		List<Estudiante> listaEstudiantes = estudianteService.listarEstudiante(critEstudiante);
		if (listaEstudiantes.size() == 1) {
			return listaEstudiantes.get(0);
		} else {
			return new Estudiante();
		}
	}

	@PostMapping("api/autoridad/estudiantes/registrarEstudiante")
	public RegistrationResult registrarEstudiante(@RequestBody Estudiante estudiante) {
		estudiante.setEstado(1);
		return estudianteService.registrarEstudiante(estudiante);
	}

	@PostMapping("api/autoridad/estudiantes/eliminarEstudiante")
	public RegistrationResult eliminarEstudiante(@RequestBody Estudiante estudiante) {
		return estudianteService.eliminarEstudiante(estudiante);
	}

	@PostMapping("api/autoridad/docentes/listarDocentes")
	public List<Docente> listarDocentes(@RequestBody CritDocente critDocente) {
		critDocente.setEstado(1);
		return docenteService.listarDocente(critDocente);
	}

	@GetMapping("api/autoridad/docentes/obtenerDocente/{idDocente}")
	public Docente obtenerDcoente(@PathVariable Long idDocente) {
		CritDocente critDocente = new CritDocente(idDocente, 1);
		List<Docente> listaDocente = docenteService.listarDocente(critDocente);
		if (listaDocente.size() == 1) {
			return listaDocente.get(0);
		} else {
			return new Docente();
		}
	}

	@PostMapping("api/autoridad/docentes/registrarDocente")
	public RegistrationResult registrarDocente(@RequestBody Docente docente) {
		docente.setEstado(1);
		return docenteService.registrarDocente(docente);
	}

	@PostMapping("api/autoridad/docentes/eliminarDocente")
	public RegistrationResult eliminarDocente(@RequestBody Docente docente) {
		return docenteService.eliminarDocente(docente);
	}

	@GetMapping(value = "api/print/{idUnidadAutoridad}")
	public @ResponseBody byte[] report(HttpServletResponse response, @PathVariable Long idUnidadAutoridad) throws Exception {
		System.out.println(":v");
		// get jasper template
		ClassPathResource reportResource = new ClassPathResource("reportes/prueba.jasper");
		ClassPathResource reportResourceImage = new ClassPathResource("reportes/images/umsaLogo.png");

		CritEstudianteCargo critEstudianteCargo = new CritEstudianteCargo();

		critEstudianteCargo.setEstado(1);
		critEstudianteCargo.setIdUnidadAutoridad(idUnidadAutoridad);
		List<EstudianteCargo> listaEstudianteCargo = autoridadService.listarTotalAutoridades(critEstudianteCargo);

		final Map<String, Object> parameters = new HashMap<>();
		parameters.put("logo", reportResourceImage.getInputStream());
		JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(listaEstudianteCargo);
		byte[] runReportToPdf = JasperRunManager.runReportToPdf(reportResource.getInputStream(), parameters, ds);
		return runReportToPdf;

	}
	
	@GetMapping(value = "api/print/xlsx/{idUnidadAutoridad}")
	public @ResponseBody void reportExcel(HttpServletResponse response, @PathVariable Long idUnidadAutoridad) throws Exception {
		ClassPathResource reportResource = new ClassPathResource("reportes/prueba.jasper");
		ClassPathResource reportResourceImage = new ClassPathResource("reportes/images/umsaLogo.png");
		try {
			InputStream jasperStream = reportResource.getInputStream();
			CritEstudianteCargo critEstudianteCargo = new CritEstudianteCargo();

			critEstudianteCargo.setEstado(1);
			critEstudianteCargo.setIdUnidadAutoridad(idUnidadAutoridad);
			List<EstudianteCargo> listaEstudianteCargo = autoridadService.listarTotalAutoridades(critEstudianteCargo);

			final Map<String, Object> parameters = new HashMap<>();
			parameters.put("logo", reportResourceImage.getInputStream());
			JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(listaEstudianteCargo);
			JasperPrint jasperPrint= JasperFillManager.fillReport(reportResource.getInputStream(), parameters, ds);
			
			response.setContentType("application/x-xls");
			response.setHeader("Content-Disposition", "inline; filename=cobe.xls"); 
			JRXlsExporter jrXlsExporter = new JRXlsExporter();
			final OutputStream outputStream = response.getOutputStream();
			jrXlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			jrXlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputStream);
			jrXlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
			jrXlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			jrXlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			jrXlsExporter.exportReport();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
