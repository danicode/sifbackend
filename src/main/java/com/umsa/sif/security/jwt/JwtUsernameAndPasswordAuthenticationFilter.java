package com.umsa.sif.security.jwt;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.crypto.SecretKey;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.umsa.sif.dao.AdmUsuarioDAO;
import com.umsa.sif.dao.AdmUsuarioRolDAO;
import com.umsa.sif.dao.PersonaDAO;
import com.umsa.sif.dto.AdmUsuario;
import com.umsa.sif.dto.AdmUsuarioRol;
import com.umsa.sif.dto.Persona;
import com.umsa.sif.dto.crit.CritAdmUsuario;
import com.umsa.sif.dto.crit.CritPersona;
import com.umsa.sif.util.Constantes;

import io.jsonwebtoken.Jwts;

public class JwtUsernameAndPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private final PasswordEncoder passwordEncoder;
	private final AuthenticationManager authenticationManager;
	private final JwtConfig jwtConfig;
	private final SecretKey secretKey;
	private final AdmUsuarioDAO admUsuarioDAO;
	private final PersonaDAO personaDAO;
	private final AdmUsuarioRolDAO admUsuarioRolDAO;

	public JwtUsernameAndPasswordAuthenticationFilter(AuthenticationManager authenticationManager, JwtConfig jwtConfig, SecretKey secretKey, AdmUsuarioDAO admUsuarioDAO, PersonaDAO personaDAO, AdmUsuarioRolDAO admUsuarioRolDAO, PasswordEncoder passwordEncoder) {
		this.authenticationManager = authenticationManager;
		this.jwtConfig = jwtConfig;
		this.secretKey = secretKey;
		this.admUsuarioDAO = admUsuarioDAO;
		this.personaDAO = personaDAO;
		this.admUsuarioRolDAO = admUsuarioRolDAO;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {

		try {

			UsernameAndPasswordAuthenticationRequest authenticationRequest = new ObjectMapper().readValue(request.getInputStream(), UsernameAndPasswordAuthenticationRequest.class);
			String usuario = authenticationRequest.getUsername().replace("@umsa.bo", "");
			authenticationRequest.setUsername(usuario);
			Hashtable<Object, String> env = new Hashtable<Object, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.PROVIDER_URL, "ldap://200.7.160.135:389");
			env.put(Context.SECURITY_AUTHENTICATION, "simple");
			env.put(Context.SECURITY_PRINCIPAL, String.format("uid=%s,ou=people,dc=umsa,dc=bo", usuario));
			env.put(Context.SECURITY_CREDENTIALS, authenticationRequest.getPassword());
			try {
				InitialDirContext context = new InitialDirContext(env);

				SearchControls constraints = new SearchControls();
				constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);

				NamingEnumeration<SearchResult> answer = context.search("dc=umsa,dc=bo", String.format("uid=%s", usuario), constraints);

				while (answer.hasMoreElements()) {
					Persona persona = new Persona();

					Attributes atributos = answer.next().getAttributes();
					if (atributos != null) {
						persona.setNombres(atributos.get("Nombre") != null ? atributos.get("Nombre").get().toString() : "");
						persona.setApPaterno(atributos.get("Paterno") != null ? atributos.get("Paterno").get().toString() : "");
						persona.setApMaterno(atributos.get("Materno") != null ? atributos.get("Materno").get().toString() : "");
						persona.setNroCi(atributos.get("CI") != null ? atributos.get("ci").get().toString() : "");
						persona.setDireccion(atributos.get("Direccion") != null ? atributos.get("Direccion").get().toString() : "");
						persona.setTelefonos(atributos.get("telefono") != null ? atributos.get("Telefono").get().toString() : "");
						persona.setCorreos((atributos.get("CorreoA") != null ? atributos.get("CorreoA").get().toString() : "") + "/" + (atributos.get("mail").get() != null ? atributos.get("mail").get().toString() : ""));
						persona.setTelefonos((atributos.get("Celular") != null ? atributos.get("Celular").get().toString() : "") + "/" + (atributos.get("Telefono").get() != null ? atributos.get("Telefono").get().toString() : ""));
						persona.setEstado(1);
						AdmUsuario admUsuario = new AdmUsuario(null, authenticationRequest.getUsername(), 1, passwordEncoder.encode(authenticationRequest.getPassword()));
						CritAdmUsuario critAdmUsuario = new CritAdmUsuario(usuario);
						List<AdmUsuario> listaUsuario = admUsuarioDAO.listarUsuarios(critAdmUsuario);
						if (listaUsuario.size() != 0) {
							// modificar contraseña (siempre)
							admUsuario = listaUsuario.get(0);
							admUsuario.setClave(passwordEncoder.encode(authenticationRequest.getPassword()));
							admUsuario.setUsuMod(admUsuario.getIdUsuario());
							admUsuarioDAO.actualizarUsuario(admUsuario);
						} else {
							// verificar si la persona ya existe
							CritPersona critPersona = new CritPersona();
							critPersona.setNroCi(persona.getNroCi());
							critPersona.setEstado(1);
							System.out.println(persona);
							List<Persona> listaPersona = personaDAO.listarPersona(critPersona);
							if (listaPersona.size() != 0) {
								// persona ya existe
								// crear usuario
								admUsuario.setIdPersona(listaPersona.get(0).getIdPersona());
								admUsuarioDAO.registrarUsuarioLDAP(admUsuario);
							} else {
								// redireccionamos el idPersona
								admUsuario.setIdPersona(1L);
								admUsuarioDAO.registrarUsuarioLDAP(admUsuario);
								admUsuario.setIdPersona(persona.getIdPersona());
								persona.setUsuReg(admUsuario.getIdUsuario());
								persona.setUsuMod(admUsuario.getIdUsuario());
								personaDAO.registrarPersona(persona);
								admUsuario.setIdPersona(persona.getIdPersona());
								admUsuarioDAO.actualizarUsuario(admUsuario);
							}
							
							//asignacion de roles predeterminados
							AdmUsuarioRol admUsuarioRol = new AdmUsuarioRol(admUsuario.getIdUsuario(), Constantes.ROL_ESTUDIANTE, 1, admUsuario.getIdUsuario(), admUsuario.getIdUsuario());
							admUsuarioRolDAO.registrarUsuarioRol(admUsuarioRol);

						}

					}
				}
			} catch (NamingException ne) {
				System.out.println(ne);
			}

			Authentication authentication = new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword());

			Authentication authenticate = authenticationManager.authenticate(authentication);

			return authenticate;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {

		CritAdmUsuario critAdmUsuario = new CritAdmUsuario(authResult.getName());
		AdmUsuario admUsuario = admUsuarioDAO.listarUsuarios(critAdmUsuario).get(0);

		String token = Jwts.builder().setSubject(authResult.getName()).claim("authorities", authResult.getAuthorities()).claim("id_Usuario", admUsuario.getIdUsuario()).setIssuedAt(new Date())
				.setExpiration(java.sql.Date.valueOf(LocalDate.now().plusDays(jwtConfig.getTokenExpirationAfterDays())))
				.signWith(secretKey).compact();

		response.addHeader(jwtConfig.getAuthorizationHeader(), jwtConfig.getTokenPrefix() + token);
	}
}
