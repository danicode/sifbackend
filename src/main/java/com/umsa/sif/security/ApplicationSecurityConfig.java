package com.umsa.sif.security;

import java.util.Arrays;

import javax.crypto.SecretKey;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.umsa.sif.dao.AdmUsuarioDAO;
import com.umsa.sif.dao.AdmUsuarioRolDAO;
import com.umsa.sif.dao.PersonaDAO;
import com.umsa.sif.security.jwt.JwtConfig;
import com.umsa.sif.security.jwt.JwtTokenVerifier;
import com.umsa.sif.security.jwt.JwtUsernameAndPasswordAuthenticationFilter;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {
	private final PasswordEncoder passwordEncoder;
	private final DataSource dataSource;
	
		@Value("${spring.queries.users-query}")
		private String usersQuery;
	
		@Value("${spring.queries.roles-query}")
		private String rolesQuery;

	private final SecretKey secretKey;
	private final JwtConfig jwtConfig;
	private final AdmUsuarioDAO admUsuarioDAO;
	private final AdmUsuarioRolDAO admUsuarioRolDAO;
	private final PersonaDAO personaDAO;
	

	@Autowired
	public ApplicationSecurityConfig(PasswordEncoder passwordEncoder, DataSource dataSource, SecretKey secretKey,
			JwtConfig jwtConfig, AdmUsuarioDAO admUsuarioDAO, PersonaDAO personaDAO, AdmUsuarioRolDAO admUsuarioRolDAO) {
		this.passwordEncoder = passwordEncoder;
		this.dataSource = dataSource;
		this.secretKey = secretKey;
		this.jwtConfig = jwtConfig;
		this.admUsuarioDAO = admUsuarioDAO;
		this.personaDAO = personaDAO;
		this.admUsuarioRolDAO = admUsuarioRolDAO;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().cors()
				.and()
				.addFilter(new JwtUsernameAndPasswordAuthenticationFilter(authenticationManager(), jwtConfig, secretKey,
						admUsuarioDAO, personaDAO, admUsuarioRolDAO, passwordEncoder))
				.addFilterAfter(new JwtTokenVerifier(secretKey, jwtConfig),
						JwtUsernameAndPasswordAuthenticationFilter.class)
				.authorizeRequests()
				.antMatchers("/index").permitAll()
				.anyRequest().authenticated() 
				;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(dataSource).usersByUsernameQuery(usersQuery).authoritiesByUsernameQuery(rolesQuery)
				.passwordEncoder(passwordEncoder);	
	}

	@Bean
	CorsConfigurationSource corsConfigurationSource() {
//        CorsConfiguration configuration = new CorsConfiguration();
//        configuration.setAllowedOrigins(Arrays.asList("*"));
//        configuration.setAllowedMethods(Arrays.asList("*"));
//        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//        source.registerCorsConfiguration("/**", configuration);
//        return source;

		CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowedOrigins(Arrays.asList("*")); // <-- you may change "*"
		configuration.setAllowedMethods(Arrays.asList("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH"));
		configuration.setAllowCredentials(true);
		configuration.addExposedHeader(
				"Authorization, x-xsrf-token, Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, "
						+ "Content-Type, Access-Control-Request-Method, Custom-Filter-Header");
//        configuration.setAllowedHeaders(Arrays.asList("*"));
		configuration.setAllowedHeaders(
				Arrays.asList("Accept", "Origin", "Content-Type", "Depth", "User-Agent", "If-Modified-Since",
						"Cache-Control", "Authorization", "X-Req", "X-File-Size", "X-Requested-With", "X-File-Name"));
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}

}
