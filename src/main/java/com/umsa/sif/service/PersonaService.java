package com.umsa.sif.service;

import java.util.List;

import com.umsa.sif.dto.Persona;
import com.umsa.sif.dto.crit.CritPersona;
import com.umsa.sif.util.RegistrationResult;

public interface PersonaService {
	List <Persona> listarPersona(CritPersona critPersona);
	RegistrationResult registrarPersona(Persona persona);
	RegistrationResult eliminarPersona(Persona persona);
}
	
