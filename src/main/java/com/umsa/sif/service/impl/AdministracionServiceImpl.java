package com.umsa.sif.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.umsa.sif.dao.AdmUsuarioDAO;
import com.umsa.sif.dao.AdmUsuarioRolDAO;
import com.umsa.sif.dao.AdmUsuarioUnidadDAO;
import com.umsa.sif.dto.AdmUsuario;
import com.umsa.sif.dto.AdmUsuarioRol;
import com.umsa.sif.dto.AdmUsuarioUnidad;
import com.umsa.sif.dto.crit.CritAdmUsuario;
import com.umsa.sif.dto.crit.CritAdmUsuarioRol;
import com.umsa.sif.dto.crit.CritAdmUsuarioUnidad;
import com.umsa.sif.service.AdministracionService;
import com.umsa.sif.util.Constantes;
import com.umsa.sif.util.RegistrationResult;

@Service
@Transactional
public class AdministracionServiceImpl implements AdministracionService {

	@Autowired
	private AdmUsuarioDAO admUsuarioDAO;
	@Autowired
	private AdmUsuarioRolDAO admUsuarioRolDAO;
	@Autowired
	private AdmUsuarioUnidadDAO admUsuarioUnidadDAO;

	@Override
	public List<AdmUsuario> listarUsuarios(CritAdmUsuario critAdmUsuario) {
		return admUsuarioDAO.listarUsuarios(critAdmUsuario);
	}

	@Override
	public RegistrationResult RegistrarUsuario(AdmUsuario admUsuario) {
		RegistrationResult registrationResult = new RegistrationResult();
		try {
			if (admUsuario.getIdUsuario() == null) {
				admUsuarioDAO.registrarUsuario(admUsuario);
			} else {
				admUsuarioDAO.actualizarUsuario(admUsuario);
			}
		} catch (Exception e) {
			registrationResult.setCode(0);
			registrationResult.setMessage(Constantes.REGISTRO_FALLIDO + e.getMessage());
			if(e.getMessage().contains("llave duplicada viola restricción de unicidad"))
				registrationResult.setMessage(Constantes.REGISTRO_FALLIDO + "Elija otro nombre de usuario");
		}
		return registrationResult;
	}

	@Override
	public RegistrationResult EliminarUsuario(AdmUsuario admUsuario) {
		RegistrationResult registrationResult = new RegistrationResult();
		try {
			admUsuarioDAO.eliminarUsuario(admUsuario);
		} catch (Exception e) {
			registrationResult.setCode(0);
			registrationResult.setMessage(Constantes.ELIMINACION_FALLIDA + e.getMessage());
		}
		return registrationResult;
	}

	@Override
	public List<AdmUsuarioRol> listarUsuarioRol(CritAdmUsuarioRol critAdmUsuarioRol) {
		
		return admUsuarioRolDAO.listarUsuarioRol(critAdmUsuarioRol);
	}

	@Override
	public RegistrationResult registrarUsuarioRol(AdmUsuarioRol admUsuarioRol) {
		RegistrationResult registrationResult = new RegistrationResult();
		try {
			admUsuarioRolDAO.registrarUsuarioRol(admUsuarioRol);
		} catch (Exception e) {
			registrationResult.setCode(0);
			registrationResult.setMessage(Constantes.REGISTRO_FALLIDO + e.getMessage());
		}
		return registrationResult;
	}

	@Override
	public RegistrationResult eliminarUsuarioRol(AdmUsuarioRol admUsuarioRol) {
		RegistrationResult registrationResult = new RegistrationResult();
		try {
			admUsuarioRolDAO.eliminarUsuarioRol(admUsuarioRol);
		} catch (Exception e) {
			registrationResult.setCode(0);
			registrationResult.setMessage(Constantes.ELIMINACION_FALLIDA + e.getMessage());
		}
		return registrationResult;
	}

	@Override
	public List<AdmUsuarioUnidad> listarUsuarioUnidad(CritAdmUsuarioUnidad critAdmUsuarioUnidad) {
		return admUsuarioUnidadDAO.listarUsuarioUnidad(critAdmUsuarioUnidad);
	}

	@Override
	public RegistrationResult registrarUsuarioUnidad(AdmUsuarioUnidad admUsuarioUnidad) {
		RegistrationResult registrationResult = new RegistrationResult();
		try {
			admUsuarioUnidadDAO.registrarUsuarioUnidad(admUsuarioUnidad);
		} catch (Exception e) {
			registrationResult.setCode(0);
			registrationResult.setMessage(Constantes.REGISTRO_FALLIDO + e.getMessage());
		}
		return registrationResult;
	}

	@Override
	public RegistrationResult eliminarUsuarioUnidad(AdmUsuarioUnidad admUsuarioUnidad) {
		RegistrationResult registrationResult = new RegistrationResult();
		try {
			admUsuarioUnidadDAO.eliminarUsuarioUnidad(admUsuarioUnidad);
		} catch (Exception e) {
			registrationResult.setCode(0);
			registrationResult.setMessage(Constantes.ELIMINACION_FALLIDA + e.getMessage());
		}
		return registrationResult;
	}


}
