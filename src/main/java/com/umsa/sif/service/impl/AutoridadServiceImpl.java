package com.umsa.sif.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.umsa.sif.dao.CargoEstudiantilDAO;
import com.umsa.sif.dao.DocenteCargoDAO;
import com.umsa.sif.dao.EstudianteCargoDAO;
import com.umsa.sif.dao.TipoCargoNivelDAO;
import com.umsa.sif.dao.TipoCargoNroDAO;
import com.umsa.sif.dto.CargoEstudiantil;
import com.umsa.sif.dto.DocenteCargo;
import com.umsa.sif.dto.EstudianteCargo;
import com.umsa.sif.dto.TipoCargoNivel;
import com.umsa.sif.dto.TipoCargoNro;
import com.umsa.sif.dto.crit.CritCargoEstudiantil;
import com.umsa.sif.dto.crit.CritDocenteCargo;
import com.umsa.sif.dto.crit.CritEstudianteCargo;
import com.umsa.sif.service.AutoridadService;
import com.umsa.sif.util.Constantes;
import com.umsa.sif.util.RegistrationResult;

@Service
@Transactional
public class AutoridadServiceImpl implements AutoridadService {

	@Autowired
	private EstudianteCargoDAO estudianteCargoDAO;
	@Autowired
	private DocenteCargoDAO docenteCargoDAO;
	@Autowired
	private CargoEstudiantilDAO cargoEstudiantilDAO;
	@Autowired
	private TipoCargoNivelDAO tipoCargoNivelDAO;
	@Autowired
	private TipoCargoNroDAO tipoCargoNroDAO;

	@Override
	public List<CargoEstudiantil> listarCargoEstudiantil(CritCargoEstudiantil critCargoEstudiantil) {

		return cargoEstudiantilDAO.listarCargoEstudiantil(critCargoEstudiantil);
	}

	@Override
	public List<TipoCargoNivel> listarTipoCargoNivel() {
		return tipoCargoNivelDAO.listarTipoCargoNivel();
	}

	@Override
	public List<TipoCargoNro> listarTipoCargoNro() {
		return tipoCargoNroDAO.listarTipoCargoNro();
	}

	@Override
	public List<EstudianteCargo> listarEstudianteCargo(CritEstudianteCargo critEstudianteCargo) {
		return estudianteCargoDAO.listarEstudianteCargo(critEstudianteCargo);
	}

	@Override
	public RegistrationResult registrarEstudianteCargo(EstudianteCargo estudianteCargo) {
		RegistrationResult registrationResult = new RegistrationResult();
		try {
			if (estudianteCargo.getIdEstudianteCargo() == null) {
				estudianteCargoDAO.registrarEstudianteCargo(estudianteCargo);
			} else {
				estudianteCargoDAO.actualizarEstudianteCargo(estudianteCargo);
			}
		} catch (Exception e) {
			registrationResult.setCode(0);
			registrationResult.setMessage(Constantes.REGISTRO_FALLIDO + e.getMessage());
		}
		return registrationResult;
	}

	@Override
	public RegistrationResult eliminarEstudianteCargo(EstudianteCargo estudianteCargo) {
		RegistrationResult registrationResult = new RegistrationResult();
		try {
			estudianteCargoDAO.eliminarEstudianteCargo(estudianteCargo);
		} catch (Exception e) {
			registrationResult.setCode(0);
			registrationResult.setMessage(Constantes.ELIMINACION_FALLIDA + e.getMessage());
		}
		return registrationResult;
	}

	@Override
	public List<DocenteCargo> listarDocenteCargo(CritDocenteCargo critDocenteCargo) {
		return docenteCargoDAO.listarDocenteCargo(critDocenteCargo);
	}

	@Override
	public RegistrationResult registrarDocenteCargo(DocenteCargo docenteCargo) {
		RegistrationResult registrationResult = new RegistrationResult();
		try {
			if (docenteCargo.getIdDocenteCargo() == null) {
				docenteCargoDAO.registrarDocenteCargo(docenteCargo);
			} else {
				docenteCargoDAO.actualizarDocenteCargo(docenteCargo);
			}
		} catch (Exception e) {
			registrationResult.setCode(0);
			registrationResult.setMessage(Constantes.REGISTRO_FALLIDO + e.getMessage());
		}
		return registrationResult;
	}

	@Override
	public RegistrationResult eliminarDocenteCargo(DocenteCargo docenteCargo) {
		RegistrationResult registrationResult = new RegistrationResult();
		try {
			docenteCargoDAO.eliminarDocenteCargo(docenteCargo);
		} catch (Exception e) {
			registrationResult.setCode(0);
			registrationResult.setMessage(Constantes.ELIMINACION_FALLIDA + e.getMessage());
		}
		return registrationResult;
	}

	@Override
	public List<EstudianteCargo> listarTotalAutoridades(CritEstudianteCargo critEstudianteCargo) {
		return estudianteCargoDAO.listarTotalAutoridades(critEstudianteCargo);
	}

}
