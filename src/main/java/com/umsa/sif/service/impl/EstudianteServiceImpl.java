package com.umsa.sif.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.umsa.sif.dao.EstudianteDAO;
import com.umsa.sif.dao.UnidadAcademicaDAO;
import com.umsa.sif.dto.Estudiante;
import com.umsa.sif.dto.UnidadAcademica;
import com.umsa.sif.dto.crit.CritEstudiante;
import com.umsa.sif.dto.crit.CritUnidadAcademica;
import com.umsa.sif.service.EstudianteService;
import com.umsa.sif.util.Constantes;
import com.umsa.sif.util.RegistrationResult;

@Service
@Transactional
public class EstudianteServiceImpl implements EstudianteService {

	@Autowired
	private EstudianteDAO EstudianteDAO;
	@Autowired
	private UnidadAcademicaDAO unidadAcademicaDAO;

	@Override
	public List<Estudiante> listarEstudiante(CritEstudiante critEstudiante) {
		return EstudianteDAO.listarEstudiante(critEstudiante);
	}

	@Override
	public RegistrationResult registrarEstudiante(Estudiante estudiante) {
		RegistrationResult registrationResult = new RegistrationResult();
		try {
			if (estudiante.getIdEstudiante() == null) {
				EstudianteDAO.registrarEstudiante(estudiante);
			} else {
				EstudianteDAO.actualizarEstudiante(estudiante);
			}
		} catch (Exception e) {
			registrationResult.setCode(0);
			registrationResult.setMessage(Constantes.REGISTRO_FALLIDO + e.getMessage());
		}
		return registrationResult;
	}

	@Override
	public RegistrationResult eliminarEstudiante(Estudiante estudiante) {
		RegistrationResult registrationResult = new RegistrationResult();
		try {
			EstudianteDAO.eliminarEstudiante(estudiante);
		} catch (Exception e) {
			registrationResult.setCode(0);
			registrationResult.setMessage(Constantes.ELIMINACION_FALLIDA);
		}
		return registrationResult;
	}

	@Override
	public List<UnidadAcademica> listarUnidadAcademica(CritUnidadAcademica critUnidadAcademica) {
		return unidadAcademicaDAO.listarUnidadAcademica(critUnidadAcademica);
	}

}
