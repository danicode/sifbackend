package com.umsa.sif.service.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.umsa.sif.dao.DocenteDAO;
import com.umsa.sif.dao.EstudianteDAO;
import com.umsa.sif.dao.PersonaDAO;
import com.umsa.sif.dao.UnidadAcademicaDAO;
import com.umsa.sif.dto.Docente;
import com.umsa.sif.dto.Estudiante;
import com.umsa.sif.dto.Persona;
import com.umsa.sif.dto.UnidadAcademica;
import com.umsa.sif.dto.crit.CritDocente;
import com.umsa.sif.dto.crit.CritEstudiante;
import com.umsa.sif.dto.crit.CritPersona;
import com.umsa.sif.dto.crit.CritUnidadAcademica;
import com.umsa.sif.service.EstudianteService;
import com.umsa.sif.service.WebService;
import com.umsa.sif.util.Constantes;
import com.umsa.sif.util.RegistrationResult;
import com.umsa.sif.util.RestUtil;

@Service
public class WebServiceImpl implements WebService {
	@Autowired
	EstudianteService estudianteService;

	@Autowired
	UnidadAcademicaDAO unidadAcademicaDAO;
	@Autowired
	PersonaDAO personaDAO;
	@Autowired
	EstudianteDAO estudianteDAO;
	@Autowired
	DocenteDAO docenteDAO;

	@Value("${wsDocenteURL}")
	private String wsDocenteURL;

	@Override
	public RegistrationResult registrarEstudiantePorCi(String ci, Long idUsuario) throws ParseException {
		List<Estudiante> listaEstudiante = new ArrayList<Estudiante>();
		Persona persona = new Persona();
		String idPersonaMatriculacion = "";
		Long idPersona;
		RestUtil restUtil = new RestUtil(Constantes.matriculacionUsuario, Constantes.matriculacionClave, Constantes.matriculacionUrlAuth);
		HashMap<String, Object> map = new HashMap<String, Object>();
//        map.put("idEstudiante", filtro.getIdEstudiante() != null ? filtro.getIdEstudiante() : "");
		map.put("dip", ci != null ? ci : "");
//        map.put("dip", ci != null ? ci : "");
//        map.put("nombreCompleto", filtro.getNombreCompleto() != null ? filtro.getNombreCompleto() : "");

		List<HashMap> estudiantesHashmap = restUtil.callJSONServiceForList(Constantes.matriculacionUrl + "/estudiante/listarEstudiantes", map, "POST");
		for (HashMap estudianteHashMap : estudiantesHashmap) {

			if (estudianteHashMap.get("postgrado").toString().toLowerCase().equals("no") && estudianteHashMap.get("estadoEstudiante").toString().toLowerCase().equals("activo")) {
				CritUnidadAcademica critUnidadAcademica = new CritUnidadAcademica();
				critUnidadAcademica.setIdPrograma(estudianteHashMap.get("idPrograma").toString());
				UnidadAcademica unidadAcademica = unidadAcademicaDAO.listarUnidadAcademica(critUnidadAcademica).get(0);

				Estudiante estudiante = new Estudiante(null, null, unidadAcademica.getIdUnidad(), estudianteHashMap.get("idEstudiante").toString(), 1, idUsuario, idUsuario);
				listaEstudiante.add(estudiante);
				idPersonaMatriculacion = estudianteHashMap.get("idPersona").toString();

			}
			if (listaEstudiante.size() != 0) {

				HashMap<String, String> params = new HashMap<String, String>();
				params.put("idPersona", idPersonaMatriculacion);
				HashMap<String, Object> personaHashmap = restUtil.callJSONServiceForResult(Constantes.matriculacionUrl + "/persona/detail", params, "POST");
				persona.setApPaterno(personaHashmap.get("paterno").toString());
				persona.setApMaterno(personaHashmap.get("materno").toString());
				persona.setNombres(personaHashmap.get("nombres").toString());
				persona.setNroCi(personaHashmap.get("dip").toString());
				persona.setCorreos(personaHashmap.get("correo").toString());
				persona.setTelefonos(personaHashmap.get("telefono").toString());
				persona.setDireccion(personaHashmap.get("direccion").toString());
				persona.setIdSexo(personaHashmap.get("idSexo").toString().equals("M") ? 1 : 2);
				persona.setEstado(1);
				persona.setUsuMod(idUsuario);
				persona.setUsuReg(idUsuario);
				persona.setFecNacimiento(new SimpleDateFormat("dd/MM/yyyy").parse(personaHashmap.get("fecNacimiento").toString()));
			}

		}

		if (listaEstudiante.size() != 0) {
			CritPersona critPersonaBusqueda = new CritPersona();
			critPersonaBusqueda.setNroCi(persona.getNroCi());
			critPersonaBusqueda.setEstado(1);
			List<Persona> personaBusqueda = personaDAO.listarPersona(critPersonaBusqueda);
			if (personaBusqueda.size() > 1) {
				throw new RuntimeException("Existen mas de 2 personas con el siguiente ci en la base de datos " + persona.getNroCi());
			} else {
				if (personaBusqueda.size() == 0) {
					personaDAO.registrarPersona(persona);
					idPersona = persona.getIdPersona();
				} else {
					idPersona = personaBusqueda.get(0).getIdPersona();
					// Pocos Casos
//					persona.setIdPersona(personaBusqueda.get(0).getIdPersona());
//					personaDAO.actualizarPersona(persona);
				}
			}
			for (Estudiante estudiante : listaEstudiante) {
				CritEstudiante critEstudiante = new CritEstudiante(estudiante.getRu(), 1);
				List<Estudiante> listaEstudianteBusqueda = estudianteDAO.listarEstudiante(critEstudiante);
				if (listaEstudianteBusqueda.size() > 1) {
					throw new RuntimeException("Existen mas de 2 personas con el siguiente ru en la base de datos " + estudiante.getRu());
				} else {
					if (listaEstudianteBusqueda.size() == 0) {
						estudiante.setIdPersona(idPersona);
						estudianteDAO.registrarEstudiante(estudiante);
					} else {
						// Si ya existe no hacer nada, a futuro se debe setear a 0 los que ya no esten
						// activos
//						persona.setIdPersona(personaBusqueda.get(0).getIdPersona());
//						personaDAO.actualizarPersona(persona);
					}
				}

			}
//			personaDAO.
//			SystCem.out.println(listaEstudiante.get(0).toString());
//			System.out.println(Arrays.asList(persona));

		}

		return null;
	}

	@Override
	public RegistrationResult registrarDocentePorCi(String ci, Long idUsuario) throws URISyntaxException {
		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		Map<String, Object> critDocenteMap = new HashMap<>();
		critDocenteMap.put("estado", 1);
		critDocenteMap.put("nroCi", ci);
		HttpEntity request = new HttpEntity<>(critDocenteMap);
		// Consultando El ws docente
//		ResponseEntity<String> responseEntity = restTemplate.postForEntity("http://localhost:8081/api/autoridad/docentes/listarDocentes", request, String.class);
		ResponseEntity<String> responseEntity = restTemplate.postForEntity(wsDocenteURL + "/api/ws/docentes/listarDocentes", request, String.class);
		// convertir Respuesta JSon a HashMap
		List<Map> listaDocenteMap = new ArrayList<Map>();
		try {
			listaDocenteMap = new ObjectMapper().readValue(responseEntity.getBody(), new TypeReference<List<HashMap>>() {
			});
		} catch (org.apache.http.ParseException | IOException e) {
			e.printStackTrace();
		}

		//
		List<Docente> listaDocente = new ArrayList<Docente>();
		Persona persona = new Persona();
		Long idPersona;
		// Iterando Lista Obtenida por WS
		for (Map docenteMap : listaDocenteMap) {
			// Obteniendo Solo a Docentes con cago "Docente"
			if (docenteMap.get("codCargo").toString().equals("8")) {
				// Obteniendo Unidad Academica
				CritUnidadAcademica critUnidadAcademica = new CritUnidadAcademica();
				critUnidadAcademica.setCodApePro(docenteMap.get("codApePro2").toString());

				UnidadAcademica unidadAcademica = unidadAcademicaDAO.listarUnidadAcademica(critUnidadAcademica).get(0);
				// Creando Nuevo Docente
				Docente docente = new Docente(null, null, unidadAcademica.getIdUnidad(), 1, idUsuario, idUsuario);
				// Adicioando docente a una lista
				listaDocente.add(docente);
				// Alistando Datos de Persona Docente
				persona.setApPaterno(docenteMap.get("apPaterno") == null ? "" : docenteMap.get("apPaterno").toString());
				persona.setApMaterno(docenteMap.get("apMaterno") == null ? "" : docenteMap.get("apMaterno").toString());
				persona.setApCasada(docenteMap.get("apCasada") == null ? "" : docenteMap.get("apCasada").toString());
				persona.setNombres(docenteMap.get("nombres") == null ? "" : docenteMap.get("nombres").toString());
				persona.setNroCi(docenteMap.get("nroCi") == null ? "" : docenteMap.get("nroCi").toString());
				persona.setEstado(1);
				persona.setUsuMod(idUsuario);
				persona.setUsuReg(idUsuario);

			}

		}
		// Alistando las inserciones o modificaciones en funcion a la lista docente
		if (listaDocente.size() != 0) {
			CritPersona critPersonaBusqueda = new CritPersona();
			critPersonaBusqueda.setNroCi(persona.getNroCi());
			critPersonaBusqueda.setEstado(1);
			// Buscando a persona docente a través de su ci
			List<Persona> personaBusqueda = personaDAO.listarPersona(critPersonaBusqueda);
			if (personaBusqueda.size() > 1) {
				throw new RuntimeException("Existen mas de 2 personas con el siguiente ci en la base de datos " + persona.getNroCi());
			} else {
				if (personaBusqueda.size() == 0) {
					// si no existe registrar
					personaDAO.registrarPersona(persona);
					idPersona = persona.getIdPersona();
				} else {
					idPersona = personaBusqueda.get(0).getIdPersona();
					// Pocos Casos
//					persona.setIdPersona(personaBusqueda.get(0).getIdPersona());
//					personaDAO.actualizarPersona(persona);
				}
			}
			// Iterando lista Docente
			for (Docente docente : listaDocente) {
				// busqueda de docente en funcion a su idUnidad
				CritDocente critDocente = new CritDocente();
				critDocente.setEstado(1);
				critDocente.setIdUnidad(docente.getIdUnidad());
				critDocente.setIdPersona(idPersona);
				List<Docente> listaDocenteBusqueda = docenteDAO.listarDocente(critDocente);
				if (listaDocenteBusqueda.size() > 1) {
//					throw new RuntimeException("Existen mas de 2 personas con  " + docente.getRu());
				} else {
					// si no existe el docente en la bd, se registra
					if (listaDocenteBusqueda.size() == 0) {
						docente.setIdPersona(idPersona);
						docenteDAO.registrarDocente(docente);
					} else {
						// Si ya existe no hacer nada, a futuro se debe setear a 0 los que ya no esten
						// activos
//						persona.setIdPersona(personaBusqueda.get(0).getIdPersona());
//						personaDAO.actualizarPersona(persona);
					}
				}

			}

		}

		return null;
	}

	@Override
	public List<Docente> listarDocentes2(CritDocente critDocente) throws ParseException {
		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		List<Docente> listaDocente = new ArrayList<Docente>();
		Docente docente;
		HttpEntity request = new HttpEntity<>(critDocente);

		ResponseEntity<String> responseEntity = restTemplate.postForEntity(wsDocenteURL + "/api/ws/docentes/listarDocentes2", request, String.class);

		List<Map> listaDocenteMap = new ArrayList<Map>();
		try {
			listaDocenteMap = new ObjectMapper().readValue(responseEntity.getBody(), new TypeReference<List<HashMap>>() {
			});
		} catch (org.apache.http.ParseException | IOException e) {
			e.printStackTrace();
		}

//		[codApePro, codDocente, categoria, codCategoria, codCargo, nombres, nroCi, codApePro2, apCasada, apMaterno, mes, carrera, cargo, anio, apPaterno]
		for (Map map : listaDocenteMap) {

			docente = new Docente();

			docente.setNroCi(map.get("nroCi") != null ? map.get("nroCi").toString() : null);
			docente.setApPaterno(map.get("apPaterno") != null ? map.get("apPaterno").toString() : null);
			docente.setApMaterno(map.get("apMaterno") != null ? map.get("apMaterno").toString() : null);
			docente.setApCasada(map.get("apCasada") != null ? map.get("apCasada").toString() : null);
			docente.setNombres(map.get("nombres") != null ? map.get("nombres").toString() : null);

			Calendar cal3 = Calendar.getInstance();
		
			cal3.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(map.get("fechaNacimiento").toString()));

			docente.setFechaNacimiento(cal3.getTime());

			listaDocente.add(docente);
		}

		return listaDocente;
	}

	private HttpComponentsClientHttpRequestFactory getClientHttpRequestFactory() {
		HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();

		clientHttpRequestFactory.setHttpClient(httpClient());

		return clientHttpRequestFactory;
	}

	private HttpClient httpClient() {
		CredentialsProvider credentialsProvider = new BasicCredentialsProvider();

		credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("docente_cons", "sys"));

		HttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(credentialsProvider).build();
		return client;
	}
}
