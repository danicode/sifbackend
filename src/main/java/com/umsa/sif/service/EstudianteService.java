package com.umsa.sif.service;

import java.util.List;

import com.umsa.sif.dto.Estudiante;
import com.umsa.sif.dto.UnidadAcademica;
import com.umsa.sif.dto.crit.CritEstudiante;
import com.umsa.sif.dto.crit.CritUnidadAcademica;
import com.umsa.sif.util.RegistrationResult;

public interface EstudianteService {
	List<Estudiante> listarEstudiante(CritEstudiante critEstudiante);
	RegistrationResult registrarEstudiante (Estudiante estudiante);
	RegistrationResult eliminarEstudiante (Estudiante estudiante);
	List<UnidadAcademica> listarUnidadAcademica(CritUnidadAcademica critUnidadAcademica);
}
