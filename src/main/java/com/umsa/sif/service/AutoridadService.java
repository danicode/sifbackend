package com.umsa.sif.service;

import java.util.List;

import com.umsa.sif.dto.CargoEstudiantil;
import com.umsa.sif.dto.DocenteCargo;
import com.umsa.sif.dto.EstudianteCargo;
import com.umsa.sif.dto.TipoCargoNivel;
import com.umsa.sif.dto.TipoCargoNro;
import com.umsa.sif.dto.crit.CritCargoEstudiantil;
import com.umsa.sif.dto.crit.CritDocenteCargo;
import com.umsa.sif.dto.crit.CritEstudianteCargo;
import com.umsa.sif.util.RegistrationResult;

public interface AutoridadService {
	List<EstudianteCargo> listarEstudianteCargo(CritEstudianteCargo critEstudianteCargo);

	RegistrationResult registrarEstudianteCargo(EstudianteCargo estudianteCargo);

	RegistrationResult eliminarEstudianteCargo(EstudianteCargo estudianteCargo);

	List<DocenteCargo> listarDocenteCargo(CritDocenteCargo critDocenteCargo);

	RegistrationResult registrarDocenteCargo(DocenteCargo docenteCargo);

	RegistrationResult eliminarDocenteCargo(DocenteCargo docenteCargo);

	List<CargoEstudiantil> listarCargoEstudiantil(CritCargoEstudiantil critCargoEstudiantil);

	List<TipoCargoNivel> listarTipoCargoNivel();

	List<TipoCargoNro> listarTipoCargoNro();
	
	List<EstudianteCargo> listarTotalAutoridades(CritEstudianteCargo critEstudianteCargo);
}
