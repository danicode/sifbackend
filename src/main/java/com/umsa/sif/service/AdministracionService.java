package com.umsa.sif.service;

import java.util.List;

import com.umsa.sif.dto.AdmUsuario;
import com.umsa.sif.dto.AdmUsuarioRol;
import com.umsa.sif.dto.AdmUsuarioUnidad;
import com.umsa.sif.dto.crit.CritAdmUsuario;
import com.umsa.sif.dto.crit.CritAdmUsuarioRol;
import com.umsa.sif.dto.crit.CritAdmUsuarioUnidad;
import com.umsa.sif.util.RegistrationResult;

public interface AdministracionService {
	List<AdmUsuario> listarUsuarios(CritAdmUsuario critAdmUsuario);

	RegistrationResult RegistrarUsuario(AdmUsuario admUsuario);

	RegistrationResult EliminarUsuario(AdmUsuario admUsuario);

	List<AdmUsuarioRol> listarUsuarioRol(CritAdmUsuarioRol critAdmUsuarioRol);

	RegistrationResult registrarUsuarioRol(AdmUsuarioRol admUsuarioRol);

	RegistrationResult eliminarUsuarioRol(AdmUsuarioRol admUsuarioRol);

	List<AdmUsuarioUnidad> listarUsuarioUnidad(CritAdmUsuarioUnidad critAdmUsuarioUnidad);

	RegistrationResult registrarUsuarioUnidad(AdmUsuarioUnidad admUsuarioUnidad);

	RegistrationResult eliminarUsuarioUnidad(AdmUsuarioUnidad admUsuarioUnidad);
}
