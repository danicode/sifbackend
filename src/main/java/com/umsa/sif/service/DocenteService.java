package com.umsa.sif.service;

import java.util.List;

import com.umsa.sif.dto.Docente;
import com.umsa.sif.dto.crit.CritDocente;
import com.umsa.sif.util.RegistrationResult;

public interface DocenteService {
	List<Docente> listarDocente(CritDocente critDocente);
	RegistrationResult registrarDocente (Docente estudiante);
	RegistrationResult eliminarDocente (Docente estudiante);
}
