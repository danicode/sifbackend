package com.umsa.sif.service;


import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;

import com.umsa.sif.dto.Docente;
import com.umsa.sif.dto.crit.CritDocente;
import com.umsa.sif.util.RegistrationResult;

public interface WebService {
	RegistrationResult registrarEstudiantePorCi(String ci, Long idUsuario) throws ParseException;
	RegistrationResult registrarDocentePorCi(String ci, Long idUsuario) throws URISyntaxException;
	List <Docente> listarDocentes2 (CritDocente critDocente) throws ParseException ;
//	List <Ws> listarPersona(CritPersona critPersona);
//	RegistrationResult registrarPersona(Persona persona);
//	RegistrationResult eliminarPersona(Persona persona);
}
	
