package com.umsa.sif.service;

import java.util.List;

import com.umsa.sif.dto.AdmUsuario;
import com.umsa.sif.dto.crit.CritAdmUsuario;

public interface MainService {
	List <AdmUsuario> listarUsuario(CritAdmUsuario critAdmUsuario);
}
	
